//
//  HistoryPager.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class HistoryPager: ButtonBarPagerTabStripViewController{
    
    
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = .darkGray
        settings.style.buttonBarItemBackgroundColor = .darkGray
        settings.style.selectedBarBackgroundColor = .orange
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.selectedBarHeight = 2
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .lightGray
            newCell?.label.textColor = .white
        }
        super.viewDidLoad()
        
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        var childs = [UIViewController]()
        childs = [PayHistoryVC.instantiate(IndicatorInfo(title: "외래결제내역")),
        MiddlePayHistoryVC.instantiate(IndicatorInfo(title: "입원중간금결제내역"))]
        
        return childs
    }
}
