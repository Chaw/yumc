//
//  PayResultTV.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayResultTV: UITableView, UITableViewDataSource, UITableViewDelegate{
    
//    var list: Payment?
    var list: [String:Any]?

    var primaryCode: String?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "PayResultCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 300
        self.rowHeight = UITableViewAutomaticDimension
        
        if list != nil {
            print("list is \(list!)")
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list == nil{
            return 0
        } else {
            print("list is \(list!)")
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PayResultCell
        cell.methodLabel.text = list!["LGD_AUTH_TYPE"] as! String?
        cell.payDateLabel.text = list!["LGD_TIMESTAMP"] as! String?
        cell.approveLabel.text = list!["LGD_FINANCEAUTHNUM"] as! String?
        cell.numLabel.text = list!["LGD_OID"] as! String?
//        cell.payLabel.text = list!["LGD_AMOUNT"] as! String?
        let pay = list!["LGD_AMOUNT"] as! String?
        let payment = Int(pay!)
        cell.payLabel.text = payment?.addComma()
        cell.bankLabel.text = list!["LGD_FINANCENAME"] as! String?
        cell.cardNumLabel.text = list!["cardno"] as! String?
        cell.monLabel.text = list!["LGD_CARDINSTALLMONTH"] as! String?
        
        let isEqual = (cell.monLabel.text == "00")
        
        if( isEqual )
        {
            cell.monLabel.text = "일시불";
        }
        else{
            cell.monLabel.text = cell.monLabel.text!+"개월";
        }
        
        cell.resultLabel.text = list!["resmsg"] as! String?
        
        if(cell.resultLabel.text=="결제성공"){
            
        }
        else{
            cell.resultLabel.textColor = UIColor.red
        }

        return cell
    }
    
}

