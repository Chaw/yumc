//
//  PayChildWebview.swift
//  yumc
//
//  Created by Wenly on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class PayChildWebview: UIViewController, UIWebViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var postData: String?
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let parameters = [
            "CST_MID": "lgdacomxpay",
            "CST_PLATFORM": "CST_PLATFORM",
            "LGD_NOINT": "0",
            "LGD_KVPMISPAUTOAPPYN": "A",
            "LGD_MONEPAY_AUTORUNYN": "N",
            "LGD_PRODUCTINFO": "외래 진료비 결제",
            "LGD_OID": "2017021615151520000126IPDG",
            "LGD_BUYER": "최학수",
            "LGD_BUYERID": "TEST02",
            "LGD_BUYEREMAIL": "",
            "LGD_AMOUNT": "1004",
            "LGD_CUSTOM_FIRSTPAY": "SC0010",
            "LGD_EASYPAY_ONLY": "",
            "LGD_INSTALL": "00",
            "PROC_DATE": "20170210",
            "SEQ_NO": "7",
            "USERNO": "20000126",
            "CLNC_DEPT_CDE": "PDG",
            "PAY_TYPE": "L"
        ]
      
        //let parameters = ["pcTest": "pTest"]
//        let urlPath :String = "http://url/test.r"
//        Alamofire.request(.POST, urlPath, parameters: parameters)
//            .response { request, response, data, error in
//                println(request)
//                println(response)
//                println(error)
//                println(data)
//        }
        
        
        
//        let url = NSURL(string: "http://yumcpay.yu.ac.kr/app/lguplus/payreq_crossplatform.php")!
        
//        var request = URLRequest(url: URL(string: "http://yumcpay.yu.ac.kr/app/lguplus/payreq_crossplatform.php")!)
//        request.httpMethod = "POST"
//        let postString = String(format: "CST_MID=%@&CST_PLATFORM=%@&LGD_NOINT=%@&LGD_KVPMISPAUTOAPPYN=%@&LGD_MONEPAY_AUTORUNYN=%@&LGD_PRODUCTINFO=%@&LGD_OID=%@&LGD_BUYER=%@&LGD_BUYERID=%@&LGD_BUYEREMAIL=%@&LGD_AMOUNT=%@&LGD_CUSTOM_FIRSTPAY=%@&LGD_EASYPAY_ONLY=%@&LGD_INSTALL=%@&PROC_DATE=%@&SEQ_NO=%@&USERNO=%@&CLNC_DEPT_CDE=%@&PAY_TYPE=%@",
//                                "lgdacomxpay",
//                                "CST_PLATFORM",
//                                "0",
//                                "A",
//                                "N",
//                                "외래 진료비 결제",
//                                "2017021615151520000126IPDG",
//                                "최학수",
//                                "TEST02",
//                                "",
//                                "1004",
//                                "SC0010",
//                                "",
//                                "00",
//                                "20170210",
//                                "7",
//                                "20000126",
//                                "PDG",
//                                "L");
//        let encodedString = postString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//        //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        
//        print(encodedString);
//        
//        request.httpBody = encodedString?.data(using: .utf8)
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data, error == nil else {                                                 // check for fundamental networking error
//                print("error=\(error)")
//                return
//            }
//            
//            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(response)")
//            }
//            
//            let responseString: String?
//            responseString = String(data: data, encoding: .utf8)
//            
//            if responseString != nil {
//                self.webViewMain.loadHTMLString(responseString!, baseURL: URL(string:"http://yumcpay.yu.ac.kr/app/")!)
//            }
//            
//        }
//        task.resume()
        
        
//        Alamofire.request(.POST, url, parameters: parameters, headers: headers, encoding: .URLEncodedInURL).response { request, response, data, error in
//            print(request)
//            print(response)
//            print(data)
//            print(error)
//        }
        
       
        
//        Intent intent = new Intent(getActivity(), LgPayActivity.class);
//        intent.putExtra("CST_MID", getString(R.string.LGD_CST_MID)); // MID : lgdacomxpay
//        intent.putExtra("CST_PLATFORM", getString(R.string.LGD_CST_PLATFORM)); // CST_PLATFORM
//        intent.putExtra("LGD_NOINT", "0"); // 무이자 할부 0:일반할부 1:무이자할부
//        intent.putExtra("LGD_KVPMISPAUTOAPPYN", getString(A);  // 동기/비동기 A:동기(A) N:동기(N) Y:비동기
//            intent.putExtra("LGD_MONEPAY_AUTORUNYN", getString(N);  // 페이나우 자동 구동 여부
//            intent.putExtra("LGD_PRODUCTINFO", "외래 진료비 결제");  // 결제명
//            intent.putExtra("LGD_OID", gMap.get("oid")); // 날짜시분초+환자번호+IorO+진료과  2017021615151520000126IPDG
//            intent.putExtra("LGD_BUYER", gMap.get("usernm")); // 환자명 최학수
//            intent.putExtra("LGD_BUYERID", gUserId); // 로그인ID TEST02
//            intent.putExtra("LGD_BUYEREMAIL", "");  // 이메일 ""
//            intent.putExtra("LGD_AMOUNT", gMap.get("amount"));  // 결제할 금액 1004
//            intent.putExtra("LGD_CUSTOM_FIRSTPAY", sFirstPay);  // 결제수단 신용카드(SC0010)
//            intent.putExtra("LGD_EASYPAY_ONLY", sPayNow);  // 페이나우이용(PAYNOW) or ""
//            intent.putExtra("LGD_INSTALL", gMap.get("halbu"));  // 할부개월 00:
//            intent.putExtra("PROC_DATE", gMap.get("procdate")); // 수납요청일자 20170210
//            intent.putExtra("SEQ_NO", gMap.get("seqno"));      // 순번 7
//            intent.putExtra("USERNO", gMap.get("userno"));     // 환자등록번호 20000126
//            intent.putExtra("CLNC_DEPT_CDE", gMap.get("clncdeptcde"));     // 진료과 : PDG
//            intent.putExtra("PAY_TYPE", "L");
//        
//        postData += "1";
//        postData += "2";
//        
//        print(postData);
        
//        intent.putExtra("CST_MID", getString(R.string.LGD_CST_MID)); // MID
//        intent.putExtra("CST_PLATFORM", getString(R.string.LGD_CST_PLATFORM)); //
//        intent.putExtra("LGD_NOINT", "0"); // 무이자 할부 0:일반할부 1:무이자할부
//        intent.putExtra("LGD_KVPMISPAUTOAPPYN", getString(R.string.LGD_KVPMISPAUTOAPPYN));  // 동기/비동기 A:동기(A) N:동기(N) Y:비동기
//        intent.putExtra("LGD_MONEPAY_AUTORUNYN", getString(R.string.LGD_MONEPAY_AUTORUNYN));  // 페이나우 자동 구동 여부
//        intent.putExtra("LGD_PRODUCTINFO", getString(R.string.LGD_PRODUCTINFO));  // 결제명
//        intent.putExtra("LGD_OID", gMap.get("oid")); // 날짜시분초
//        intent.putExtra("LGD_BUYER", gMap.get("usernm")); // 환자명
//        intent.putExtra("LGD_BUYERID", gUserId); // 로그인ID
//        intent.putExtra("LGD_BUYEREMAIL", "");  // 이메일
//        intent.putExtra("LGD_AMOUNT", gMap.get("amount"));  // 결제할 금액
//        intent.putExtra("LGD_CUSTOM_FIRSTPAY", sFirstPay);  // 결제수단
//        intent.putExtra("LGD_EASYPAY_ONLY", sPayNow);  // 페이나우이용
//        intent.putExtra("LGD_INSTALL", gMap.get("halbu"));  // 할부개월
//        intent.putExtra("PROC_DATE", gMap.get("procdate")); // 수납요청일자
//        intent.putExtra("SEQ_NO", gMap.get("seqno"));      // 순번
//        intent.putExtra("USERNO", gMap.get("userno"));     // 환자등록번호
//        intent.putExtra("CLNC_DEPT_CDE", gMap.get("clncdeptcde"));     // 환자등록번호
//        intent.putExtra("PAY_TYPE", "L");
//            
//        postData = "CST_MID=" + URLEncoder.encode(intent.getExtras().getString("CST_MID"));
//        postData += "&CST_PLATFORM=" + URLEncoder.encode(intent.getExtras().getString("CST_PLATFORM"));
//        postData += "&LGD_NOINT=" + URLEncoder.encode(intent.getExtras().getString("LGD_NOINT"));
//        postData += "&LGD_INSTALL=" + URLEncoder.encode(intent.getExtras().getString("LGD_INSTALL"));
//        postData += "&LGD_KVPMISPAUTOAPPYN=" + URLEncoder.encode(intent.getExtras().getString("LGD_KVPMISPAUTOAPPYN"));
//        postData += "&LGD_MONEPAY_AUTORUNYN=" + URLEncoder.encode(intent.getExtras().getString("LGD_MONEPAY_AUTORUNYN"));
//        postData += "&LGD_PRODUCTINFO=" + URLEncoder.encode(intent.getExtras().getString("LGD_PRODUCTINFO"));
//        postData += "&LGD_OID=" + URLEncoder.encode(intent.getExtras().getString("LGD_OID"));
//        postData += "&LGD_BUYER=" + URLEncoder.encode(intent.getExtras().getString("LGD_BUYER"));
//        postData += "&LGD_BUYERID=" + URLEncoder.encode(intent.getExtras().getString("LGD_BUYERID"));
//        postData += "&LGD_BUYEREMAIL=" + URLEncoder.encode(intent.getExtras().getString("LGD_BUYEREMAIL"));
//        postData += "&LGD_AMOUNT=" + URLEncoder.encode(intent.getExtras().getString("LGD_AMOUNT"));
//        postData += "&LGD_EASYPAY_ONLY=" + URLEncoder.encode(intent.getExtras().getString("LGD_EASYPAY_ONLY"));
//        postData += "&LGD_CUSTOM_FIRSTPAY=" + URLEncoder.encode(intent.getExtras().getString("LGD_CUSTOM_FIRSTPAY"));
//        /** 추가 전송 */
//        postData += "&PROC_DATE=" + URLEncoder.encode(intent.getExtras().getString("PROC_DATE"));
//        postData += "&SEQ_NO=" + URLEncoder.encode(intent.getExtras().getString("SEQ_NO"));
//        postData += "&CTNO=" + URLEncoder.encode(intent.getExtras().getString("USERNO"));
//        if("M".equals(sPayType)){
//            postData += "&ADM_CNT=" + URLEncoder.encode(intent.getExtras().getString("ADM_CNT")); // 입원회차
//            postData += "&INSR_CLAS_CHNG_SEQ=" + URLEncoder.encode(intent.getExtras().getString("INSR_CLAS_CHNG_SEQ")); //
//            postData += "&INSR_CLAS_CDE=" + URLEncoder.encode(intent.getExtras().getString("INSR_CLAS_CDE")); // 환자구분코드
//        }else{
//            postData += "&CLNC_DEPT_CDE=" + URLEncoder.encode(intent.getExtras().getString("CLNC_DEPT_CDE")); // 진료과코드
//            Log.d("LgFragment", "clncdeptcde ::: " + intent.getExtras().getString("CLNC_DEPT_CDE"));
//        }
//        postData += "&PAY_TYPE=" + URLEncoder.encode(intent.getExtras().getString("PAY_TYPE"));
//        /** 추가 전송 끝*/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }

    @IBOutlet weak var webViewMain: UIWebView!
    
}
