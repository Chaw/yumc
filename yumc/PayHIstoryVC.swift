//
//  PayHIstoryVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PayHistoryVC: UIViewController, IndicatorInfoProvider{
    
    var itemInfo: IndicatorInfo = "외래결제내역"
    
    static func instantiate(_ itemInfo: IndicatorInfo) -> PayHistoryVC{
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "payHistoryVC") as! PayHistoryVC
        vc.itemInfo = itemInfo
        return vc
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
