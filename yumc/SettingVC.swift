//
//  SettingVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 25..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class SettingVC: UIViewController{
    
    @IBOutlet weak var idSaveSwitch: UISwitch!
    @IBOutlet weak var autoLoginSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        idSaveSwitch.isOn = UserDefaults.standard.value(forKey: "saveId") as! Bool!
        autoLoginSwitch.isOn = UserDefaults.standard.value(forKey: "autoLogin") as! Bool!
        
        if(UserData.loginMethod == "guest"){
            idSaveSwitch.isHidden = true;
            autoLoginSwitch.isHidden = true;
            labelIDSave.isHidden = true;
            labelAutoLogin.isHidden = true;
            buttonLogout.isHidden = true;
            buttonLogoutGuest.isHidden = false;
        }
        else{
            idSaveSwitch.isHidden = false;
            autoLoginSwitch.isHidden = false;
            labelIDSave.isHidden = false;
            labelAutoLogin.isHidden = false;
            buttonLogout.isHidden = false;
            buttonLogoutGuest.isHidden = true;
        }
    }
    
    @IBAction func goToLoginVC(_ sender: Any) {
        
        UserData.logout()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.autoLogined = true
        self.present(vc, animated: true, completion: nil)
//        self.dismiss(animated: false, completion: nil)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.homeVC?.dismiss(animated: true)
    }
    
    static func popUpSetting(_ vc: UIViewController){
        let settingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingVC") as! SettingVC
        settingVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        settingVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.present(settingVC, animated: true, completion: nil)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveId(_ sender: UISwitch) {
        sender.isOn = !sender.isOn
        UserDefaults.standard.set(sender.isOn, forKey: "saveId")
    }
    @IBAction func autoLogin(_ sender: UISwitch) {
        sender.isOn = !sender.isOn
        UserDefaults.standard.set(sender.isOn, forKey: "autoLogin")
    }
    
    
    @IBOutlet weak var labelIDSave: UILabel!
    
    @IBOutlet weak var labelAutoLogin: UILabel!
    
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var buttonLogoutGuest: UIButton!
}
