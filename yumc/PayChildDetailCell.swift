//
//  PayChildDetailCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayChildDetailCell: UITableViewCell{
    
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var depLabel: UILabel!
    @IBOutlet weak var payLabel: UILabel!
    @IBOutlet weak var monPicker: UIPickerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
