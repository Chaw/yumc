//
//  MiidlePayLastTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayLastTV: UITableView, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    
    var list: Payment2?
    var month: Int = 0
    
    let data = ["일시불", "2개월", "3개월", "4개월", "5개월", "6개월", "7개월", "8개월", "9개월", "10개월", "11개월", "12개월", "38개월"]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "MiddlePayLastCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 100
        self.rowHeight = UITableViewAutomaticDimension
        if list != nil {
            print(list!)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list == nil{
            return 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiddlePayLastCell
        cell.startDateLabel.text = list!.DUR_START?.makeDateForm()
        cell.endDateLabel.text = list!.DUR_END?.makeDateForm()
        cell.insuranceLabel.text = list!.INSR_CLAS_CDE_NM?.decodeUri()
        cell.usernameLabel.text = UserData.usr_name
        if let chrgAmt = Int(list!.CHRG_AMT!){
            cell.chrgAmtLabel.text = chrgAmt.addComma()
        } else {
            cell.chrgAmtLabel.text = ""
        }
        if let recvAmt = Int(list!.RECV_AMT!){
            cell.recvAmtLabel.text = recvAmt.addComma()
        } else {
            cell.recvAmtLabel.text = ""
        }
        if let payAmt = Int(list!.AMNG_AMT!){
            cell.amngAmtLabel.text = payAmt.addComma()
        } else {
            cell.amngAmtLabel.text = ""
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.monPicker.delegate = self
        cell.monPicker.dataSource = self
        return cell
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        label.textColor = .darkGray
        label.textAlignment = .right
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        label.text = data[row]
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        month = row
        print(month)
    }
}

