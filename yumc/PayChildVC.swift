//
//  PayChildVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayChildVC: UIViewController, SendPayData{
    
    @IBOutlet weak var payChildDetailTV: PayChildDetailTV!
    @IBOutlet weak var payChildTV: PayChildTV!
    @IBOutlet weak var hideTVButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        payChildTV.sendData = self
        payChildTV.parentVC = self
        hideTVButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        hideTVButton.isHidden = true
        containerView.isHidden = true
    }
    
    @IBAction func hideDetailTV(_ sender: Any) {
        hideTVButton.isHidden = true
        containerView.isHidden = true
        
        payChildTV.list = [Payment]()
        payChildTV.reloadData()
        payChildTV.load()
    }
    
    func sendData(_ data: Payment) {
        payChildDetailTV.parent = self;
        payChildDetailTV.list = data
        payChildDetailTV.reloadData()
        containerView.isHidden = false
        hideTVButton.isHidden = false
    }
    @IBAction func popSetting(_ sender: Any) {
        SettingVC.popUpSetting(self)
    }
    
    
    @IBAction func popPaynow(_ sender: Any) {
        let vc = ViewControllerPayWeb(nibName: "ViewControllerPayWeb", bundle: nil)
        let p:Payment? = payChildDetailTV.list
        
        vc.dept_NAME = p?.DEPT_NAME
        vc.proc_DATE = p?.ACT_DATE
        vc.ctno = p?.CTNO
        vc.pay_ACT_AMT = p?.PAY_ACT_AMT
        vc.proc_DATE = p?.PROC_DATE
        vc.seq_NO = p?.SEQ_NO
        vc.clnc_DEPT_CDE = p?.CLNC_DEPT_CDE
        vc.card_DATE_TIME = p?.CARD_DATE_TIME
        vc.card_APRV_NO = p?.CARD_APRV_NO
        vc.card_NAME = p?.CARD_NAME
        vc.card_MON = Int32(payChildDetailTV.month)
        vc.rcpt_DATE = p?.RCPT_DATE
        vc.pay_TYPE = "L"
        vc.treat_TYPE = "O"
        vc.update_DATE = payChildTV.updateDate
        vc.easypay_ONLY = "PAYNOW"
        
        vc.userid = UserData.loginId
        vc.username = UserData.usr_name
        
        // Present View "Modally"
        self.parent?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func popPayWebView(_ sender: Any) {
        // Register Nib
        let vc = ViewControllerPayWeb(nibName: "ViewControllerPayWeb", bundle: nil)
        let p:Payment? = payChildDetailTV.list
        
        vc.dept_NAME = p?.DEPT_NAME
        vc.proc_DATE = p?.ACT_DATE
        vc.ctno = p?.CTNO
        vc.pay_ACT_AMT = p?.PAY_ACT_AMT
        vc.proc_DATE = p?.PROC_DATE
        vc.seq_NO = p?.SEQ_NO
        vc.clnc_DEPT_CDE = p?.CLNC_DEPT_CDE
        vc.card_DATE_TIME = p?.CARD_DATE_TIME
        vc.card_APRV_NO = p?.CARD_APRV_NO
        vc.card_NAME = p?.CARD_NAME
        vc.card_MON = Int32(payChildDetailTV.month)
        vc.rcpt_DATE = p?.RCPT_DATE
        vc.pay_TYPE = "L"
        vc.treat_TYPE = "O"
        vc.update_DATE = payChildTV.updateDate
        vc.easypay_ONLY = ""
        
        vc.userid = UserData.loginId
        vc.username = UserData.usr_name
        
        // Present View "Modally"
        self.parent?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionCallL(_ sender: Any) {

        if let url = URL(string:"telprompt://053-655-1098"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBOutlet weak var tvMsg: UITextView!
}
