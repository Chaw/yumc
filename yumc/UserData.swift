//
//  UserData.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

class UserData {
    static let KEY_SAVE_ID = "saveId"
    static let KEY_AUTO_LOGIN = "autoLogin"
    static let KEY_LOGIN_ID = "loginId"
    static let KEY_LOGIN_PW = "loginPw"
    static let KEY_LOGIN_PATIENT_NUMBER = "patientNumber"
    static let KEY_USERNO = "userno"
    static let KEY_USER_NAME = "usr_name"
    
    static let KEY_LOGIN_METHOD = "loginMethod"
    
    static var isSaveId : Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_SAVE_ID)
        }
        get {
            return getBool(KEY_SAVE_ID)
        }
    }

    static var isAutoLogin : Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_AUTO_LOGIN)
        }
        get {
            return getBool(KEY_AUTO_LOGIN)
        }
    }
    
    static var loginId : String? {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_LOGIN_ID)
        }
        get {
            return getString(KEY_LOGIN_ID)
        }
    }
    static var loginPw : String? {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_LOGIN_PW)
        }
        get {
            return getString(KEY_LOGIN_PW)
        }
    }
    static var patientNumber : String? {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_LOGIN_PATIENT_NUMBER)
        }
        get {
            return getString(KEY_LOGIN_PATIENT_NUMBER)
        }
    }
    
    static var userno : String! {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_USERNO)
        }
        get {
            if let v = getString(KEY_USERNO) {
                return v
            } else {
                return ""
            }
        }
    }

    static var usr_name : String! {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_USER_NAME)
        }
        get {
            if let v = getString(KEY_USER_NAME) {
                return v
            } else {
                return ""
            }
        }
    }

    static func setLogin(_ success: Bool!, loginId: String?, loginPw: String?, userno: String?, usr_name: String?) {
        if success == true {
            UserData.loginId = loginId
            UserData.loginPw = loginPw
            UserData.userno = userno
            UserData.usr_name = usr_name?.decodeUri()
            UserData.loginMethod = "normal"
        }
        else {
            UserData.loginId = nil
            UserData.loginPw = nil
            UserData.userno = nil
            UserData.usr_name = nil
            UserData.loginMethod = nil
        }
    }
    
    static func setLogin(_ success: Bool!, patientNumber: String?, userno: String?, usr_name: String?) {
        if success == true {
            UserData.patientNumber = patientNumber
            UserData.userno = userno
            UserData.usr_name =  usr_name?.decodeUri()
            UserData.loginMethod = "guest"
        }
        else {
            UserData.patientNumber = nil
            UserData.userno = nil
            UserData.usr_name = nil
            UserData.loginMethod = nil
        }
    }
    
    static func logout(){
        UserData.loginPw = nil
        UserDefaults.standard.set(false, forKey: KEY_AUTO_LOGIN)
    }
    
    private static func getString(_ key: String!) -> String?{
        if let value = UserDefaults.standard.value(forKey: key) as? String{
            return value
        } else {
            UserDefaults.standard.set(nil, forKey: key)
            return nil
        }
    }
    
    private static func getBool(_ key: String!) -> Bool{
        if let value = UserDefaults.standard.value(forKey: key) as! Bool!{
            return value
        } else {
            UserDefaults.standard.set(false, forKey: key)
            return false
        }
    }
    
    static var loginMethod : String? {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_LOGIN_METHOD)
        }
        get {
            return getString(KEY_LOGIN_METHOD)
        }
    }
    
}
