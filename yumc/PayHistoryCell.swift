//
//  PayHistoryCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayHistoryCell: UITableViewCell{
    
    @IBOutlet weak var ACT_DATE: UILabel!
    @IBOutlet weak var DEPT_NAME: UILabel!
    @IBOutlet weak var CARD_DATE_TIME: UILabel!
    @IBOutlet weak var CARD_APRV_NO: UILabel!
    @IBOutlet weak var CARD_NAME: UILabel!
    @IBOutlet weak var CARD_MON: UILabel!
    @IBOutlet weak var PAY_ACT_AMT: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
