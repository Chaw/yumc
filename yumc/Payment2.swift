//
//  Payment2.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

//{
//    "DUR_START": "20170111",
//    "DUR_END": "20170113",
//    "INSR_CLAS_CDE": "D4",
//    "INSR_CLAS_CDE_NM": "ë³´í",
//    "ADM_CNT": "4",
//    "INSR_CLAS_CHNG_SEQ": "8",
//    "CLNC_DEPT_CDE": "GSB",
//    "CHRG_AMT": "656720",
//    "RECV_AMT": "2190",
//    "AMNG_AMT": "654530",
//    "TOT_AMT": "1542520",
//    "INSR_AMT": "885800",
//    "ADM_AMT": "0",
//    "PROC_DATE": "20170113",
//    "SEQ_NO": "8"
//}

import ObjectMapper

class Payment2: Mappable{
    
    var DUR_START: String?
    var DUR_END: String?
    var INSR_CLAS_CDE: String?
    var INSR_CLAS_CDE_NM: String?
    var ADM_CNT: String?
    var INSR_CLAS_CHNG_SEQ: String?
    var CLNC_DEPT_CDE: String?
    var CHRG_AMT: String?
    var RECV_AMT: String?
    var AMNG_AMT: String?
    var TOT_AMT: String?
    var INSR_AMT: String?
    var ADM_AMT: String?
    var PROC_DATE: String?
    var SEQ_NO: String?
    var CTNO: String?
    var READ_TIME: String?
    var LIMIT_TIME: String?
    
    func mapping(map: Map) {
        DUR_START <- map["DUR_START"]
        DUR_END <- map["DUR_END"]
        INSR_CLAS_CDE <- map["INSR_CLAS_CDE"]
        INSR_CLAS_CDE_NM <- map["INSR_CLAS_CDE_NM"]
        ADM_CNT <- map["ADM_CNT"]
        INSR_CLAS_CHNG_SEQ <- map["INSR_CLAS_CHNG_SEQ"]
        CLNC_DEPT_CDE <- map["CLNC_DEPT_CDE"]
        CHRG_AMT <- map["CHRG_AMT"]
        RECV_AMT <- map["RECV_AMT"]
        AMNG_AMT <- map["AMNG_AMT"]
        TOT_AMT <- map["TOT_AMT"]
        INSR_AMT <- map["INSR_AMT"]
        ADM_AMT <- map["ADM_AMT"]
        PROC_DATE <- map["PROC_DATE"]
        SEQ_NO <- map["SEQ_NO"]
        CTNO <- map["CTNO"]
        READ_TIME <- map["READ_TIME"]
        LIMIT_TIME <- map["LIMMIT_TIME"]
    }
    
    required init?(map: Map) {
    }
}
