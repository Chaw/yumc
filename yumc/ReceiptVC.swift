//
//  ReceiptVC.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class ReceiptVC: UIViewController{
    
    
    @IBOutlet weak var webView: UIWebView!
    
    var payment: Payment!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 50.0/255, green: 58.0/255, blue: 165.0/255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]

    }
    
    override func viewDidAppear(_ animated: Bool) {
//        https://yumcpay.yu.ac.kr/app/mReceipt.php?userno=20004832&proc_date=201701225&seq_no=1&clnc_dept_cde=IMP&rcpt_date=20170206
        let url = "\(Url.RECEIPT)?userno=\(UserData.userno!)&proc_date=\(payment.PROC_DATE!)&seq_no=\(payment.SEQ_NO!)&clnc_dept_cde=\(payment.CLNC_DEPT_CDE!)&rcpt_date=\(payment.RCPT_DATE!)"
        print(url)
        
        webView.loadRequest(URLRequest(url: URL(string: url)!))
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
