//
//  PayChildDetailTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayChildDetailTV: UITableView, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate{
    
    var list: Payment?
    var primaryCode: String?
    var parent: PayChildVC?
    let data = ["일시불", "2개월", "3개월", "4개월", "5개월", "6개월", "7개월", "8개월", "9개월", "10개월", "11개월", "12개월", "38개월"]
    var month: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "PayChildDetailCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 300
        self.rowHeight = UITableViewAutomaticDimension
        
        let currentDateTime = Date()
        // get the user's calendar
        let userCalendar = Calendar.current
        // choose which date and time components are needed
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        let year = dateTimeComponents.year!
        let stringYear = String(year)
        let month = dateTimeComponents.month!
        var stringMonth: String
        if month < 10 {
            stringMonth = "0\(month)"
        } else {
            stringMonth = String(month)
        }
        let day = dateTimeComponents.day!
        var stringDay: String
        if day < 10 {
            stringDay = "0\(day)"
        } else {
            stringDay = String(day)
        }
        var stringHour: String
        let hour = dateTimeComponents.hour!
        if hour < 10 {
            stringHour = "0\(hour)"
        } else {
            stringHour = String(hour)
        }
        var stringMinute: String
        let minute = dateTimeComponents.minute!
        if minute < 10 {
            stringMinute = "0\(minute)"
        } else {
            stringMinute = String(minute)
        }
        var stringSecond: String
        let second = dateTimeComponents.second!
        if second < 10{
            stringSecond = "0\(second)"
        } else {
            stringSecond = String(second)
        }
        
        primaryCode = stringYear + stringMonth + stringDay + stringHour + stringMinute + stringSecond
        
        if list != nil {
            print(list!)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list == nil{
            return 0
        } else {
            return 1
//            return data!.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PayChildDetailCell
//        cell.numLabel.text = (list?.ACT_DATE)! + UserData.userno + "O" + (list?.CLNC_DEPT_CDE)!
        cell.numLabel.text = primaryCode! + UserData.userno + "O" + (list?.CLNC_DEPT_CDE)!
        cell.nameLabel.text = UserData.usr_name
        cell.dateLabel.text = list?.ACT_DATE!.makeDateForm()
        cell.depLabel.text = list?.DEPT_NAME
        cell.payLabel.text = Int(list!.PAY_ACT_AMT!)?.addComma()
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.monPicker.delegate = self
        cell.monPicker.dataSource = self
        return cell
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        label.textColor = .darkGray
        label.textAlignment = .right
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        label.text = data[row]
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        month = row
        print(month)
    }

}
