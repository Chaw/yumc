//
//  HomeVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class HomeVC: UIViewController{
    
    @IBOutlet weak var containerView: UIView!
    var data: [String:Any]?
    var payType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("userno=\(UserData.userno), usr_name=\(UserData.usr_name)")
//        let payResultVC = self.storyboard?.instantiateViewController(withIdentifier: "payResultVC") as! PayResultVC
//        payResultVC.data = data
//        changeChild(payResultVC)
        if data != nil{
            let payResultVC = self.storyboard?.instantiateViewController(withIdentifier: "payResultVC") as! PayResultVC
            payResultVC.data = data
            payResultVC.payType = payType
            changeChild(payResultVC)
        } else {
            let homeChildVC = self.storyboard?.instantiateViewController(withIdentifier: "homeChildVC") as! HomeChildVC
            changeChild(homeChildVC)
        }
        
        
    }
    
    @IBAction func addHomeChildVC(_ sender: Any) {
        
        
        let homeChildVC = self.storyboard?.instantiateViewController(withIdentifier: "homeChildVC") as! HomeChildVC
        removeChild()
        changeChild(homeChildVC)
    }
    
    @IBAction func addPayChildVC(_ sender: Any) {
        
        
        let payChildVC = self.storyboard?.instantiateViewController(withIdentifier: "payChildVC") as! PayChildVC
        removeChild()
        changeChild(payChildVC)
    }
    
    @IBAction func addMiddlePayChildVC(_ sender: Any) {
        
        
        let middlePayChildVC = self.storyboard?.instantiateViewController(withIdentifier: "middlePayChildVC") as! MiddlePayChildVC
        removeChild()
        changeChild(middlePayChildVC)

    }
    
    @IBAction func addHistoryChildVC(_ sender: Any) {
        
        
        let historyChildVC = self.storyboard?.instantiateViewController(withIdentifier: "historyChildVC") as! HistoryChildVC
        removeChild()
        changeChild(historyChildVC)
    }
    
    func removeChild(){
        let vc = self.childViewControllers.last
        vc?.view.removeFromSuperview()
        vc?.removeFromParentViewController()
    }
    
    func changeChild(_ vc: UIViewController){
        self.addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height)
        self.containerView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
//    @IBOutlet weak var buttonCallL: UIButton!
//    
//    @IBOutlet weak var buttonCallM: UIButton!
//    
//    @IBAction func actionCallL(_ sender: Any) {
//        
//        if let url = URL(string:"telprompt://053-655-1098"), UIApplication.shared.canOpenURL(url) {
//            UIApplication.shared.openURL(url)
//        }
//    }
//    
//    @IBAction func actionCallM(_ sender: Any) {
//        if let url = URL(string:"telprompt://053-620-4527"), UIApplication.shared.canOpenURL(url) {
//            UIApplication.shared.openURL(url)
//        }
//    }
}
