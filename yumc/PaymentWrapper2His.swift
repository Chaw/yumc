//
//  PaymentWrapper2His.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

import ObjectMapper

class PaymentWrapper2His: Mappable {
    var PTNM: String?
    var ListData: [Payment2His]?
    var result_code: String?
    var result_msg: String?
    
    func mapping(map: Map) {
        PTNM <- map["PTNM"]
        ListData <- map["ListData"]
        result_code <- map["result_code"]
        result_msg <- map["result_msg"]
    }
    
    required init?(map: Map) {
    }
    
    static func getPaymentHistory(
        _ usrno:String!,
        completion: @escaping (_ object:PaymentWrapper2His) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
        param["userno"] = usrno
        API.getRequest(Url.M_PAY_HISTORY,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
}
