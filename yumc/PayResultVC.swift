//
//  PayResultVC.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayResultVC: UIViewController{
    
    @IBOutlet weak var payResultTV: PayResultTV!
    @IBOutlet weak var btnBack: UIButton!
    
    var data: [String:Any]?
    var payType: String?
    
//    NSString* pay_type = [datas objectAtIndex:0];
//    NSString* rsltCode = [datas objectAtIndex:1];
//    NSString* resmsg = [datas objectAtIndex:2];
//    NSString* LGD_AUTH_TYPE = [datas objectAtIndex:3];
//    NSString* LGD_TIMESTAMP = [datas objectAtIndex:4];
//    NSString* LGD_FINANCEAUTHNUM = [datas objectAtIndex:5];
//    NSString* LGD_OID = [datas objectAtIndex:6];
//    NSString* LGD_AMOUNT = [datas objectAtIndex:7];
//    NSString* LGD_FINANCENAME = [datas objectAtIndex:8];
//    NSString* cardno = [datas objectAtIndex:9];
//    NSString* LGD_CARDINSTALLMONTH = [datas objectAtIndex:10];
    
//    var pay_type: String?
//    var rsltCode: String?
//    var resmsg: String?
//    var LGD_AUTH_TYPE: String?
//    var LGD_TIMESTAMP: String?
//    var LGD_FINANCEAUTHNUM: String?
//    var LGD_OID: String?
//    var LGD_AMOUNT: String?
//    var LGD_FINANCENAME: String?
//    var cardno: String?
//    var LGD_CARDINSTALLMONTH: String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.transform = CGAffineTransform(scaleX: -1, y: 1)
        
//        payChildTV.list = [Payment]()
        payResultTV.list = data
        payResultTV.reloadData()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.homeVC?.dismiss(animated: false)
//    }
    
    @IBAction func btnBack(_ sender: Any) {
        back()
    }
    
    @IBAction func popSetting(_ sender: Any) {
        SettingVC.popUpSetting(self)
    }
    
    @IBAction func confirm(_ sender: Any) {
        back()
    }
    
    func back(){
        if(self.payType == "L"){
            let parent = self.parent as! HomeVC
            parent.addPayChildVC(Any.self)
        }
        else{
            let parent = self.parent as! HomeVC
            parent.addMiddlePayChildVC(Any.self)
        }
        
        
    }
    
    
    
}
