//
//  CustomerLoginVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class CustomerLoginVC: UIViewController{
    
    @IBOutlet weak var patientNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var birthTF: UITextField!
    @IBOutlet weak var autoLoginImage: UIImageView!
    @IBOutlet weak var saveInfoImage: UIImageView!
    var autoLoginBool: Bool?
    var saveInfo: Bool?
    
    override func viewDidLoad(){
        super.viewDidLoad()
//        checkUserDefault()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        checkSaveInfo()
//        checkAutoLogin()
//    }
    
    @IBAction func numberDel(_ sender: Any) {
        patientNumberTF.text = ""
    }
    @IBAction func nameDel(_ sender: Any) {
        nameTF.text = ""
    }
    @IBAction func birthDel(_ sender: Any) {
        birthTF.text = ""
    }
    
//    @IBAction func autoLogin(_ sender: Any) {
//        UserData.isAutoLogin = !autoLoginBool!
//        autoLoginBool = !autoLoginBool!
//        checkAutoLogin()
//    }
    
//    @IBAction func saveInfo(_ sender: Any) {
//        UserData.isSaveId = !saveInfo!
//        saveInfo = !saveInfo!
//        checkSaveInfo()
//    }
    
    @IBAction func loginVC(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        vc.autoLogined = true
        self.present(vc, animated: false, completion: nil)
    }
    
//    func checkAutoLogin(){
//        if autoLoginBool! {
//            autoLoginImage.image = UIImage(named: "circle_check_s.png")
//        } else {
//            autoLoginImage.image = UIImage(named: "circle_check_n.png")
//        }
//    }
//    func checkSaveInfo(){
//        if saveInfo! {
//            saveInfoImage.image = UIImage(named: "circle_check_s.png")
//            if let patientNumber = UserData.patientNumber {
//                patientNumberTF.text = patientNumber
//            }
//        } else {
//            saveInfoImage.image = UIImage(named: "circle_check_n.png")
//        }
//    }
//    
//    func checkUserDefault(){
//        saveInfo = UserData.isSaveId
//        autoLoginBool = UserData.isAutoLogin
//    }

    @IBAction func actionLoginGuest(_ sender: Any) {
        
        if((patientNumberTF.text?.characters.count)! < 6){
            let alert = UIAlertController(title: "알림", message: "환자등록번호를 입력해주세요.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if((nameTF.text?.characters.count)! < 2){
            let alert = UIAlertController(title: "알림", message: "이름을 입력해주세요.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else if((birthTF.text?.characters.count)! != 8){
            let alert = UIAlertController(title: "알림", message: "생년월일을 입력해주세요.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            Login.loginWithGuest(
                patientNumberTF.text,
                usernm: nameTF.text,
                birth: birthTF.text,
                completion: { (object:Login) in
                    if object.result_code == "100"{
                        UserData.setLogin(true, patientNumber: self.patientNumberTF.text, userno: object.usr_no, usr_name: object.usr_name)
                        self.toMain()
                    }
                    else {
                        UserData.setLogin(false, patientNumber: self.patientNumberTF.text, userno: nil, usr_name: nil)
                        
                        let alert = UIAlertController(title: "알림", message: object.result_msg?.decodeUri(), preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
            }, indicator: self.view)
        }
        
    }
    
    func toMain() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
        vc?.modalTransitionStyle = .crossDissolve;
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.homeVC = vc
        self.present(vc!, animated: true, completion: nil)
    }
}
