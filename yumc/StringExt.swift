//
//  StringExt.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

extension String {
    func decodeUri() -> String?{
        let str = self.replacingOccurrences(of: "+", with: " ")
        return str.removingPercentEncoding
    }
    
    func makeDateForm() -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let readableDate = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "yyyy.MM.dd"
        let timeStamp = dateFormatter.string(from: readableDate!)
        return timeStamp
    }
    
    func makeDatetimeForm() -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        let readableDate = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "yyyy.MM.dd HH:mm:ss"
        let timeStamp = dateFormatter.string(from: readableDate!)
        return timeStamp
    }
}
