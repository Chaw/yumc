//
//  IntExt.swift
//  yumc
//
//  Created by 조지훈 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

extension Int{
    func addComma() -> String?{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let string = numberFormatter.string(from: NSNumber(value: self))
        return string
    }
}
