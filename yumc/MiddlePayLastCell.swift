//
//  MiddlePayLastCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayLastCell: UITableViewCell{
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var insuranceLabel: UILabel!
    @IBOutlet weak var chrgAmtLabel: UILabel!
    @IBOutlet weak var recvAmtLabel: UILabel!
    @IBOutlet weak var amngAmtLabel: UILabel!
    @IBOutlet weak var monPicker: UIPickerView!
    
//    let data = ["일시불", "2개월", "3개월", "4개월", "5개월", "6개월", "7개월", "8개월", "9개월", "10개월", "11개월", "12개월", "38개월"]
    
//    var month: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        monPicker.delegate = self
//        monPicker.dataSource = self
    }
    
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//        var label: UILabel
//        if let view = view as? UILabel {
//            label = view
//        } else {
//            label = UILabel()
//        }
//        label.textColor = .darkGray
//        label.textAlignment = .right
//        label.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
//        label.text = data[row]
//        return label
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return data.count
//    }
//    
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        month = row
//        print(month)
//    }
}

