//
//  Pay1.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

//    "DEPT_NAME": "\ud638\ud761\uae30\uc9c4\ub8cc\uc13c\ud130",
//    "ACT_DATE": "20170125",
//    "CTNO": "20004832",
//    "PAY_ACT_AMT": "191140",
//    "PROC_DATE": "20170125",
//    "SEQ_NO": "1"


import ObjectMapper

class Payment: Mappable{
    var DEPT_NAME: String?
    var ACT_DATE: String?
    var CTNO: String?
    var PAY_ACT_AMT: String?
    var PROC_DATE: String?
    var SEQ_NO: String?
    var CLNC_DEPT_CDE: String?
    
    var CARD_DATE_TIME: String?
    var CARD_APRV_NO: String?
    var CARD_NAME: String?
    var CARD_MON: String?
    var RCPT_DATE: String?
    var READ_TIME: String?
    var LIMIT_TIME: String?
    
    func mapping(map: Map) {
        DEPT_NAME <- map["DEPT_NAME"]
        ACT_DATE <- map["ACT_DATE"]
        CTNO <- map["CTNO"]
        PAY_ACT_AMT <- map["PAY_ACT_AMT"]
        PROC_DATE <- map["PROC_DATE"]
        SEQ_NO <- map["SEQ_NO"]
        CLNC_DEPT_CDE <- map["CLNC_DEPT_CDE"]
        
        CARD_DATE_TIME <- map["CARD_DATE_TIME"]
        CARD_APRV_NO <- map["CARD_APRV_NO"]
        CARD_NAME <- map["CARD_NAME"]
        CARD_MON <- map["CARD_MON"]
        CLNC_DEPT_CDE <- map["CLNC_DEPT_CDE"]
        RCPT_DATE <- map["RCPT_DATE"]
        
        READ_TIME <- map["READ_TIME"]
        LIMIT_TIME <- map["LIMMIT_TIME"]
    }
    
    required init?(map: Map) {
    }
}
