//
//  LoginUser.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import ObjectMapper

//url	https://yumcpay.yu.ac.kr/app/mLogin.php?usr_id=test1&usr_pw=1
//in	usr_id		아이디
//usr_pw		비밀번호
//out	{"usr_no":"17899872","usr_name":"%EC%A0%84%EB%91%90%EC%97%B0","usr_born":null,"usr_tel":null,"usr_sex":null,"result_code":"100","result_msg":"%EC%A0%95%EC%83%81%EC%B2%98%EB%A6%AC"}		결과데이타
//usr_no		환자등록번호
//result_code		결과코드	100:정상처리, 200:처리실패
//result_msg		결과메세지
//usr_tel		전화번호
//usr_sex		성별
//usr_born		생년월일
//usr_name		환자명

//{
//    "usr_no": "17899872",
//    "usr_name": "ì ëì°",
//    "usr_born": null,
//    "usr_tel": null,
//    "usr_sex": null,
//    "result_code": "100",
//    "result_msg": "ì ìì²ë¦¬"
//}

class Login: Mappable{
    var usr_no: String?
    var usr_name: String?
    var usr_born: String?
    var usr_tel: String?
    var usr_sex: String?
    var result_code: String?
    var result_msg: String?
    
    func mapping(map: Map) {
        usr_no <- map["usr_no"]
        usr_name <- map["usr_name"]
        usr_born <- map["usr_born"]
        usr_tel <- map["usr_tel"]
        usr_sex <- map["usr_sex"]
        result_code <- map["result_code"]
        result_msg <- map["result_msg"]
    }
    
    required init?(map: Map) {
    }
    
    static func loginWithUser(
        _ usr_id:String!,
        usr_pw:String!,
        completion: @escaping (_ object:Login) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
        param["usr_id"] = usr_id
        param["usr_pw"] = usr_pw
        API.getRequest(Url.LOGIN_USER,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
    
    static func loginWithGuest(
        _ userno:String!,
        usernm:String!,
        birth:String!,
        completion: @escaping (_ object:Login) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
        param["userno"] = userno
        param["usernm"] = usernm
        param["birth"] = birth
        API.getRequest(Url.LOGIN_GUEST,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
    
}
