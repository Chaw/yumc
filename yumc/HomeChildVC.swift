//
//  HomeChildVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class HomeChildVC: UIViewController{
    
    @IBOutlet weak var cardBackImage: UIImageView!
    @IBOutlet weak var barcodeContainerView: UIView!
    var cardHidden = false
    @IBOutlet weak var patNumLabel: UILabel!
    @IBOutlet weak var patNameLabel1: UILabel!
    @IBOutlet weak var patNameLabel2: UILabel!
    @IBOutlet weak var barcodeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barcodeContainerView.layer.cornerRadius = 10.0
        cardBackImage.layer.cornerRadius = 10.0
        patNameLabel1.text = UserData.usr_name
        patNameLabel2.text = UserData.usr_name
        patNumLabel.text = UserData.userno
        let image = Barcode.fromString(patNumLabel.text!, scaleX:20, scaleY:20)
        barcodeImage.image = image!
    }
    
    @IBAction func cardHidden(_ sender: Any) {
        if cardHidden {
            cardBackImage.isHidden = !cardHidden
        } else {
            cardBackImage.isHidden = !cardHidden
        }
        cardHidden = !cardHidden
    }
    @IBAction func popBarcode(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "popBarcodeVC") as! PopBarcodeVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.barcodeString = patNumLabel.text
        self.parent!.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func popSetting(_ sender: Any) {
        SettingVC.popUpSetting(self)
    }
    
}
