//
//  ViewControllerPayWeb.m
//  yumc
//
//  Created by Wenly on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

// 0. 김동훈 차장이 직접 문의하라 해서 연락함.
// 1. 파라미터 Encoding 방식 - ifou 와 yumc 서버 연동 비교, ifou 기준으로 개발했었음.
// 2. 콜백 시 javascript 연동방식
// 3. Post, Get 방식 (login 포함)



#import "ViewControllerPayWeb.h"

#import "yumc-Swift.h"

#define NSEUCKRStringEncoding -2147481280
#define sampleParameterAndroid @"CST_MID=lgdacomxpay&CST_PLATFORM=CST_PLATFORM&LGD_NOINT=0&LGD_INSTALL=00&LGD_KVPMISPAUTOAPPYN=A&LGD_MONEPAY_AUTORUNYN=N&LGD_PRODUCTINFO=%EC%9E%85%ED%87%B4%EC%9B%90+%EC%A4%91%EA%B0%84%EA%B8%88+%EC%88%98%EB%82%A9+%EA%B2%B0%EC%A0%9C&LGD_OID=2017021622091613879504INS&LGD_BUYER=%EC%A0%95%EC%9C%A1%EC%A3%BC&LGD_BUYERID=TEST02&LGD_BUYEREMAIL=&LGD_AMOUNT=1004&LGD_EASYPAY_ONLY=&LGD_CUSTOM_FIRSTPAY=SC0010&PROC_DATE=20170118&SEQ_NO=3&CTNO=13879504&ADM_CNT=3&INSR_CLAS_CHNG_SEQ=3&INSR_CLAS_CDE=D1&PAY_TYPE=M"

@interface ViewControllerPayWeb ()

@end

@implementation ViewControllerPayWeb

//#define storeURL @"http://yachawm.cafe24.com/lgpay/payreq_crossplatform.php"
#define storeURL @"https://yumcpay.yu.ac.kr/app/lguplus/payreq_crossplatform.php"
//#define storeURL @"https://yumcpay.yu.ac.kr/app/lguplus/payreq_crossplatform_s.php"
//#define storeURL @"https://yumcpay.yu.ac.kr/app/lguplus/sample_crossplatform.html"
//#define storeURL @"http://page.ifou.co.kr/lguplus/payreq_crossplatform.php"
//#define storeURL @"http://www.naver.com"


- (void)viewDidLoad
{
    [super viewDidLoad];
    //상점 URL 세팅 해주세요
    //    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:storeURL]];
    //    [webview loadRequest:request];
    if ([storeURL isEqualToString:@"http://<상점URL>"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"#define storeURL에 해당 상점 URL을 입력해주세요" message:nil delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
        [alert show];
    }
    
//    intent.putExtra("CST_MID", getString(R.string.LGD_CST_MID)); // MID
//    intent.putExtra("CST_PLATFORM", getString(R.string.LGD_CST_PLATFORM)); //
//    intent.putExtra("LGD_NOINT", "0"); // 무이자 할부 0:일반할부 1:무이자할부
//    intent.putExtra("LGD_KVPMISPAUTOAPPYN", getString(R.string.LGD_KVPMISPAUTOAPPYN));  // 동기/비동기 A:동기(A) N:동기(N) Y:비동기
//    intent.putExtra("LGD_MONEPAY_AUTORUNYN", getString(R.string.LGD_MONEPAY_AUTORUNYN));  // 페이나우 자동 구동 여부
//    intent.putExtra("LGD_PRODUCTINFO", getString(R.string.LGD_PRODUCTINFO));  // 결제명
//    intent.putExtra("LGD_OID", gMap.get("oid")); // 날짜시분초
//    intent.putExtra("LGD_BUYER", gMap.get("usernm")); // 환자명
//    intent.putExtra("LGD_BUYERID", gUserId); // 로그인ID
//    intent.putExtra("LGD_BUYEREMAIL", "");  // 이메일
//    intent.putExtra("LGD_AMOUNT", gMap.get("amount"));  // 결제할 금액
//    intent.putExtra("LGD_CUSTOM_FIRSTPAY", sFirstPay);  // 결제수단
//    intent.putExtra("LGD_EASYPAY_ONLY", sPayNow);  // 페이나우이용
//    intent.putExtra("LGD_INSTALL", gMap.get("halbu"));  // 할부개월
//    intent.putExtra("PROC_DATE", gMap.get("procdate")); // 수납요청일자
//    intent.putExtra("SEQ_NO", gMap.get("seqno"));      // 순번
//    intent.putExtra("USERNO", gMap.get("userno"));     // 환자등록번호
//    intent.putExtra("CLNC_DEPT_CDE", gMap.get("clncdeptcde"));     // 병원 과코드
//    intent.putExtra("PAY_TYPE", "L");
    
    
//    @property (nonatomic, retain) NSString *DEPT_NAME;
//    @property (nonatomic, retain) NSString *CTNO;
//    @property (nonatomic, retain) NSString *PAY_ACT_AMT;
//    @property (nonatomic, retain) NSString *PROC_DATE;
//    @property (nonatomic, retain) NSString *SEQ_NO;
//    @property (nonatomic, retain) NSString *CLNC_DEPT_CDE;
//    @property (nonatomic, retain) NSString *CARD_DATE_TIME;
//    @property (nonatomic, retain) NSString *CARD_APRV_NO;
//    @property (nonatomic, retain) NSString *CARD_NAME;
//    @property (nonatomic, retain) NSString *CARD_MON;
//    @property (nonatomic, retain) NSString *RCPT_DATE;
//    
    NSURL *url = [NSURL URLWithString: storeURL];
    
    NSString *CST_MID = [@"lgdacomxpay" stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]; // - 고정
    
    NSString *CST_PLATFORM = @"CST_PLATFORM"; // - 고정
    
    NSString *LGD_INSTALL = @"00";//self.CARD_MON;//@"00"; // 할부개월
    
    if(self.CARD_MON == 0){
        LGD_INSTALL = @"00";
    }
    else if(self.CARD_MON < 12){
        LGD_INSTALL = [NSString stringWithFormat:@"%02d",self.CARD_MON+1];
    }
    else{
        LGD_INSTALL = @"38";
    }
    
    NSString *LGD_KVPMISPAUTOAPPYN = @"A"; // 동기/비동기 A:동기(A) N:동기(N) Y:비동기 - 고정
    
    NSString *LGD_MONEPAY_AUTORUNYN = @"N"; // 페이나우 자동 구동 여부 - 고정
    
    NSString *LGD_PRODUCTINFO = @"외래 진료비"; // 결제명
    
    NSDate *date = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyyMMddHHmmss"];
    
    NSString *dateString = [df stringFromDate:date];
    
    NSString *LGD_OID =  [[NSString stringWithFormat:@"I%@%@%@%@",dateString,self.CTNO,self.TREAT_TYPE, self.CLNC_DEPT_CDE ] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//@"2017021615151520000126IPDG";//self.LGD_OID;//@"2017021615151520000126IPDG"; // 날짜시분초+PDG(병원 과코드)
    
    NSString *LGD_BUYER = self.USERNAME;//@"최학수"; // 환자명
    
    NSString *LGD_BUYERID = self.USERID;//@"TEST02"; // 로그인ID
    
    NSString *LGD_BUYEREMAIL = @""; // 이메일
    
    NSString *LGD_AMOUNT = self.PAY_ACT_AMT;//@"35700"; // 결제할 금액
    
    NSString *LGD_EASYPAY_ONLY = [NSString stringWithFormat:@"%@",self.EASYPAY_ONLY]; // 페이나우이용 - 일단 공백
    
    NSString *LGD_CUSTOM_FIRSTPAY = @"SC0010"; // 결제수단 - 고정
    
    NSString *LGD_NOINT = @"0"; // 무이자 할부 0:일반할부 1:무이자할부 - 고정
    
    
    NSString *PROC_DATE = self.PROC_DATE;//@"20170210"; // 수납요청일자
    
    NSString *SEQ_NO = self.SEQ_NO;//@"7"; // 순번
    
    NSString *CTNO = self.CTNO;//@"20000126"; // 환자등록번호
    
    if([self.PAY_TYPE isEqualToString:@"L"]){
        self.imagaViewTitleIcon.image = [UIImage imageNamed:@"icon_card_view"];
    }
    
    //NSString *PAY_KIND = @"L"; // 결제 종류 - 고정
    
    //NSMutableString *body = [[NSMutableString alloc]initWithFormat:@"CST_MID=%@&CST_PLATFORM=%@&LGD_NOINT=%@&LGD_KVPMISPAUTOAPPYN=%@&LGD_MONEPAY_AUTORUNYN=%@&LGD_PRODUCTINFO=%@&LGD_OID=%@&LGD_BUYER=%@&LGD_BUYERID=%@&LGD_BUYEREMAIL=%@&LGD_AMOUNT=%@&LGD_CUSTOM_FIRSTPAY=%@&LGD_EASYPAY_ONLY=%@&LGD_INSTALL=%@&PROC_DATE=%@&SEQ_NO=%@&USERNO=%@&PAY_TYPE=%@&LGD_TIMESTAMP=1234567890", CST_MID, @"service", @"0", @"A", @"N", @"진료비수납", LGD_OID, @"홍길동", LGD_BUYERID, @"", @"500", @"SC0010", @"", @"00", PROC_DATE, SEQ_NO, CTNO, self.PAY_TYPE];
    
    NSMutableString *body = [[NSMutableString alloc]initWithFormat:@"CST_MID=%@&CST_PLATFORM=%@&LGD_NOINT=%@&LGD_KVPMISPAUTOAPPYN=%@&LGD_MONEPAY_AUTORUNYN=%@&LGD_PRODUCTINFO=%@&LGD_OID=%@&LGD_BUYER=%@&LGD_BUYERID=%@&LGD_BUYEREMAIL=%@&LGD_AMOUNT=%@&LGD_CUSTOM_FIRSTPAY=%@&LGD_EASYPAY_ONLY=%@&LGD_INSTALL=%@&PROC_DATE=%@&SEQ_NO=%@&CTNO=%@&USERNO=%@&PAY_TYPE=%@&LGD_KVPMISPWAPURL=%@", CST_MID, CST_PLATFORM, LGD_NOINT, LGD_KVPMISPAUTOAPPYN, LGD_MONEPAY_AUTORUNYN, LGD_PRODUCTINFO, LGD_OID, LGD_BUYER, LGD_BUYERID, LGD_BUYEREMAIL, LGD_AMOUNT, LGD_CUSTOM_FIRSTPAY, LGD_EASYPAY_ONLY, LGD_INSTALL, PROC_DATE, SEQ_NO, CTNO, CTNO, self.PAY_TYPE,@"gaonsolutionYumcScheme"];
    
//    self.ADM_CNT = @"3";
//    self.INSR_CLAS_CHNG_SEQ = @"3";
//    self.INSR_CLAS_CDE = @"D1";
//    
    if([self.PAY_TYPE isEqualToString:@"M"]){
        [body appendString:[NSString stringWithFormat:@"&ADM_CNT=%@&INSR_CLAS_CHNG_SEQ=%@&INSR_CLAS_CDE=%@",self.ADM_CNT, self.INSR_CLAS_CHNG_SEQ, self.INSR_CLAS_CDE]];
    }
    else{
        //[body appendString:[NSString stringWithFormat:@"&CLNC_DEPT_CDE=%@",self.CLNC_DEPT_CDE]];
    }
    
    //[body appendString:[NSString stringWithFormat:@"&ADM_CNT=%@&INSR_CLAS_CHNG_SEQ=%@&INSR_CLAS_CDE=%@",self.ADM_CNT, self.INSR_CLAS_CHNG_SEQ, self.INSR_CLAS_CDE]];

    [body appendString:[NSString stringWithFormat:@"&CLNC_DEPT_CDE=%@",self.CLNC_DEPT_CDE]];
    
    //body = sampleParameterAndroid;
    
    NSLog(@"body = %@",body);
    
    //NSString *CLNC_DEPT_CDE = self.CLNC_DEPT_CDE;//@"PDG"; // 병원 과코드
    
    
    
    //NSMutableString *body = [NSString stringWithString:postString.string];
    
//    NSMutableString *body = [NSString stringWithFormat:@"CST_MID=%@&CST_PLATFORM=%@&LGD_NOINT=%@&LGD_KVPMISPAUTOAPPYN=%@&LGD_MONEPAY_AUTORUNYN=%@&LGD_PRODUCTINFO=%@&LGD_OID=%@&LGD_BUYER=%@&LGD_BUYERID=%@&LGD_BUYEREMAIL=%@&LGD_AMOUNT=%@&LGD_CUSTOM_FIRSTPAY=%@&LGD_EASYPAY_ONLY=%@&LGD_INSTALL=%@&PROC_DATE=%@&SEQ_NO=%@&USERNO=%@&CLNC_DEPT_CDE=%@&PAY_TYPE=%@", CST_MID, CST_PLATFORM, LGD_NOINT, LGD_KVPMISPAUTOAPPYN, LGD_MONEPAY_AUTORUNYN, LGD_PRODUCTINFO, LGD_OID, LGD_BUYER, LGD_BUYERID, LGD_BUYEREMAIL, LGD_AMOUNT, LGD_CUSTOM_FIRSTPAY, LGD_EASYPAY_ONLY, LGD_INSTALL, PROC_DATE, SEQ_NO, CTNO, CLNC_DEPT_CDE, PAY_KIND];
//        @"CST_MID=lgdacomxpay&CST_PLATFORM=CST_PLATFORM&LGD_NOINT=0&LGD_KVPMISPAUTOAPPYN=A&LGD_MONEPAY_AUTORUNYN=N&LGD_PRODUCTINFO=%EC%99%B8%EB%9E%98%20%EC%A7%84%EB%A3%8C%EB%B9%84%20%EA%B2%B0%EC%A0%9C&LGD_OID=2017021615151520000126IPDG&LGD_BUYER=%EC%B5%9C%ED%95%99%EC%88%98&LGD_BUYERID=TEST02&LGD_BUYEREMAIL=&LGD_AMOUNT=1004&LGD_CUSTOM_FIRSTPAY=SC0010&LGD_EASYPAY_ONLY=&LGD_INSTALL=00&PROC_DATE=20170210&SEQ_NO=7&USERNO=20000126&CLNC_DEPT_CDE=PDG&PAY_TYPE=L";
//    //@"CST_MID=lgdacomxpay&CST_PLATFORM=CST_PLATFORM&LGD_NOINT=0&LGD_INSTALL=00&LGD_KVPMISPAUTOAPPYN=A&LGD_MONEPAY_AUTORUNYN=N&LGD_PRODUCTINFO=%EC%9E%85%ED%87%B4%EC%9B%90+%EC%A4%91%EA%B0%84%EA%B8%88+%EC%88%98%EB%82%A9+%EA%B2%B0%EC%A0%9C&LGD_OID=2017021622091613879504INS&LGD_BUYER=%EC%A0%95%EC%9C%A1%EC%A3%BC&LGD_BUYERID=TEST02&LGD_BUYEREMAIL=&LGD_AMOUNT=1004&LGD_EASYPAY_ONLY=&LGD_CUSTOM_FIRSTPAY=SC0010&PROC_DATE=20170118&SEQ_NO=3&CTNO=13879504&ADM_CNT=3&INSR_CLAS_CHNG_SEQ=3&INSR_CLAS_CDE=D1&PAY_TYPE=M";
//
    
    NSData *postData = [body dataUsingEncoding: NSUTF8StringEncoding];
    //NSData *postData = [body dataUsingEncoding: NSASCIIStringEncoding];NSEUCKRStringEncoding;NSUTF8StringEncoding;
    //NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
    NSLog(@"url = %@",url);
    [request setHTTPMethod: @"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: postData];
    [self.webview loadRequest: request];
    
    
//    NSURLResponse * response = nil;
//    NSError * error = nil;
//    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSLog(@"received data %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
//    
    
    //CST_MID=lgdacomxpay&CST_PLATFORM=CST_PLATFORM&LGD_NOINT=0&LGD_KVPMISPAUTOAPPYN=A&LGD_MONEPAY_AUTORUNYN=N&LGD_PRODUCTINFO=%EC%99%B8%EB%9E%98%20%EC%A7%84%EB%A3%8C%EB%B9%84%20%EA%B2%B0%EC%A0%9C&LGD_OID=2017021615151520000126IPDG&LGD_BUYER=%EC%B5%9C%ED%95%99%EC%88%98&LGD_BUYERID=TEST02&LGD_BUYEREMAIL=&LGD_AMOUNT=1004&LGD_CUSTOM_FIRSTPAY=SC0010&LGD_EASYPAY_ONLY=&LGD_INSTALL=00&PROC_DATE=20170210&SEQ_NO=7&USERNO=20000126&CLNC_DEPT_CDE=PDG&PAY_TYPE=L
    
    // Do any additional setup after loading the view, typically from a nib.
    
    NSDate *now = [NSDate date];
    NSTimeInterval interval = 300-[now timeIntervalSinceDate:self.UPDATE_DATE];
    
    NSLog(@"interval = %f",interval);
    
    [self performSelector:@selector(dismissWithTimeout:) withObject:self afterDelay:interval];
}

- (void)dismissWithTimeout:(id)sender{
    
    UIAlertController *alertController = [UIAlertController
                                          
                                          alertControllerWithTitle:@"알림"
                                          
                                          message:@"결제시간(5분)이 초과되었습니다.\n메인으로 이동합니다."
                                          
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               
                               actionWithTitle:@"확인"
                               
                               style:UIAlertActionStyleDefault
                               
                               handler:^(UIAlertAction *action)
                               
                               {
                                   NSString *urlString = [NSString stringWithFormat:@"%@%@",@"https://yumcpay.yu.ac.kr/app/",@"mPayFiveLimmit.php"];
                                   NSURL *url = [NSURL URLWithString:urlString];
                                   NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
                                   
                                   NSMutableString *body = [[NSMutableString alloc]initWithFormat:@"userno=%@&procdate=%@&seqno=%@",self.CTNO, self.PROC_DATE, self.SEQ_NO];
                                   
                                   NSData *postData = [body dataUsingEncoding: NSUTF8StringEncoding];
                                   //NSData *postData = [body dataUsingEncoding: NSASCIIStringEncoding];NSEUCKRStringEncoding;NSUTF8StringEncoding;
                                   //NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
                                   
                                   NSLog(@"url = %@",url);
                                   [urlRequest setHTTPMethod: @"GET"];
                                   //    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                                   //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                                   [urlRequest setHTTPBody: postData];
                                   
                                   NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                                   [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
                                   {
                                       
                                       if (error)
                                       {
                                           //NSLog(@"Error,%@", [error localizedDescription]);
                                           NSLog(@"타임아웃 기록 실패");
                                       }
                                       else 
                                       {
                                           NSLog(@"5분 타임아웃 기록 성공");
                                           //NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                                       }
                                       
                                       [self dismissViewControllerAnimated:NO completion:^{
                                           AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication]delegate];
                                           HomeVC *homeVC = (HomeVC*)appDelegate.homeVC;
                                           [homeVC addHomeChildVC:self];
                                       }];
                                       
                                   }];
                                   
                               }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissWithTimeout:) object:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 webView의 delegate method
 */
/*
 Smshinhanansimclick      // 스마트 신한 URL 스키마
 shinhan-sr-ansimclick     // 신마트 신한앱 URL 스키마
 smhyundaiansimclick      // 현대카드 URL 스키마
 nonghyupcardansimclick   // NH 안심클릭 URL 스키마
 paypin                    // PAYPIN URL 스키마
 ispmobile                 // ISPMOBILE URL 스키마
 */

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.indicator stopAnimating];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"didFailLoadWithError = %@",error.description);
    NSLog(@"didFailLoadWithError code = %ld",(long)error.code);
    [self.indicator stopAnimating];
    
    if(error.code==101){
        UIAlertController *alertController = [UIAlertController
                                              
                                              alertControllerWithTitle:@"알림"
                                              
                                              message:@"결제 앱 설치가 필요합니다."
                                              
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   
                                   actionWithTitle:@"확인"
                                   
                                   style:UIAlertActionStyleDefault
                                   
                                   handler:^(UIAlertAction *action)
                                   
                                   {
                                       
                                       [webView goBack];
                                       
                                       NSLog(@"OK action");
                                       
                                   }];
        
        [alertController addAction:okAction];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    UIDevice* device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)])
        backgroundSupported = device.multitaskingSupported;
    NSLog(@"backgroundSupported ==>%@",(backgroundSupported?@"YES":@"NO"));
    if (!backgroundSupported){
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"안 내"
                                                      message:@"멀티테스킹을 지원하는 기기 또는 어플만  공인인증서비스가 가능합니다."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];
        return YES;
    }
    
    NSString *reqUrl = [[request URL] absoluteString];
    NSLog(@"webview에 요청된 URL => %@", reqUrl);
    
    //    if ([reqUrl hasPrefix:@"ispmobile://"]){
    //        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"ispmobile://"]]) {
    //            // ISPMobile 호출
    //            return YES;
    //        }
    //        else{
    //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"설치하시겠습니까?" message:@"확인버튼을 누르시면 스토어로 이동합니다." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil];
    //            [alert setTag:1];
    //            [alert show];
    //            return NO;
    //        }
    //    }
    if ([reqUrl hasPrefix:@"smartxpay-transfer://"]) {
        NSLog(@"%@", reqUrl);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"smartxpay-transfer://"]]) {
            // smartxpay 호출
            [[UIApplication sharedApplication] openURL:[request URL]];
            NSLog(@"여기로 와야해");
            return NO;
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"설치하시겠습니까?" message:@"확인버튼을 누르시면 스토어로 이동합니다." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil];
            [alert setTag:2];
            [alert show];
            return NO;
        }
    }
    if ([reqUrl hasPrefix:@"paypin://MP_2APP"]) {
        NSLog(@"%@", reqUrl);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"paypin://MP_2APP"]]) {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return YES;
        }
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"jscall://"]) {
        
        NSString *requestString = [[request URL] absoluteString];
        NSArray *components = [requestString componentsSeparatedByString:@"jscall://"];
        NSString *value = [components objectAtIndex:1];
        NSLog(@"value = %@",value);
        NSString *decoded = [[value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        NSLog(@"decoded = %@",decoded);
        NSArray *datas = [decoded componentsSeparatedByString:@"|"];
        
        NSLog(@"datas = %@",datas);
        
        
//        <?=urlencode($pay_type)?>'+'|'
//        +'<?=urlencode($rsltCode)?>'+'|'
//        +'<?=urlencode($resmsg)?>'+'|'
//        +'<?=urlencode($LGD_AUTH_TYPE)?>'+'|'
//        +'<?=urlencode($LGD_TIMESTAMP)?>'+'|'
//        +'<?=urlencode($LGD_FINANCEAUTHNUM)?>'+'|'
//        +'<?=urlencode($LGD_OID)?>'+'|'
//        +'<?=urlencode($LGD_AMOUNT)?>'+'|'
//        +'<?=urlencode($LGD_FINANCENAME)?>'+'|'
//        +'<?=urlencode($cardno)?>'+'|'
//        +'<?=urlencode($LGD_CARDINSTALLMONTH)?>';
//        
        NSString* pay_type = [datas objectAtIndex:0];
        NSString* rsltCode = [datas objectAtIndex:1];
        NSString* resmsg = [datas objectAtIndex:2];
        NSString* LGD_AUTH_TYPE = [datas objectAtIndex:3];
        NSString* LGD_TIMESTAMP = [datas objectAtIndex:4];
        NSString* LGD_FINANCEAUTHNUM = [datas objectAtIndex:5];
        NSString* LGD_OID = [datas objectAtIndex:6];
        NSString* LGD_AMOUNT = [datas objectAtIndex:7];
        NSString* LGD_FINANCENAME = [datas objectAtIndex:8];
        NSString* cardno = [datas objectAtIndex:9];
        NSString* LGD_CARDINSTALLMONTH = [datas objectAtIndex:10];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        HomeVC *payVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        
        payVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        payVC.pay_type = @"pay_type";
//        payVC.rsltCode = @"rsltCode";
//        payVC.resmsg = @"한글 공백 0123 abcd @!#!%!@";
//        payVC.LGD_AUTH_TYPE = @"LGD_AUTH_TYPE";
//        payVC.LGD_TIMESTAMP = @"LGD_TIMESTAMP";
//        payVC.LGD_FINANCEAUTHNUM = @"LGD_FINANCEAUTHNUM";
//        payVC.LGD_OID = @"LGD_OID";
//        payVC.LGD_AMOUNT = @"LGD_AMOUNT";
//        payVC.LGD_FINANCENAME = @"LGD_FINANCENAME";
//        payVC.cardno = @"cardno";
//        payVC.LGD_CARDINSTALLMONTH = @"LGD_CARDINSTALLMONTH";
        
        //결제구분|결과코드|결과메세지|결제수단|결제일시|승인번호|주문고유번호|결제금액|결제기관|카드번호|할부개월
        
        payVC.data = [NSDictionary dictionaryWithObjectsAndKeys:
                      pay_type,@"pay_type",
                      rsltCode,@"rsltCode",
                      resmsg,@"resmsg",
                      LGD_AUTH_TYPE,@"LGD_AUTH_TYPE",
                      LGD_TIMESTAMP,@"LGD_TIMESTAMP",
                      LGD_FINANCEAUTHNUM,@"LGD_FINANCEAUTHNUM",
                      LGD_OID,@"LGD_OID",
                      LGD_AMOUNT,@"LGD_AMOUNT",
                      LGD_FINANCENAME,@"LGD_FINANCENAME",
                      cardno,@"cardno",
                      LGD_CARDINSTALLMONTH,@"LGD_CARDINSTALLMONTH",
                      nil];
        
        payVC.payType = @"M";//self.PAY_TYPE;
        
//        payVC.data = [NSDictionary dictionaryWithObjectsAndKeys:
//                      @"pay_type",@"pay_type",
//                      @"rsltCode",@"rsltCode",
//                      @"한글 공백 0123 abcd @!#!%!@",@"resmsg",
//                      @"LGD_AUTH_TYPE",@"LGD_AUTH_TYPE",
//                      @"LGD_TIMESTAMP",@"LGD_TIMESTAMP",
//                      @"LGD_FINANCEAUTHNUM",@"LGD_FINANCEAUTHNUM",
//                      @"LGD_OID",@"LGD_OID",
//                      @"LGD_AMOUNT",@"LGD_AMOUNT",
//                      @"LGD_FINANCENAME",@"LGD_FINANCENAME",
//                      @"cardno",@"cardno",
//                      @"LGD_CARDINSTALLMONTH",@"LGD_CARDINSTALLMONTH",
//                      nil];
        
//        payVC.pay_type = pay_type;
//        payVC.rsltCode = rsltCode;
//        payVC.resmsg = resmsg;
//        payVC.LGD_AUTH_TYPE = LGD_AUTH_TYPE;
//        payVC.LGD_TIMESTAMP = LGD_TIMESTAMP;
//        payVC.LGD_FINANCEAUTHNUM = LGD_FINANCEAUTHNUM;
//        payVC.LGD_OID = LGD_OID;
//        payVC.LGD_AMOUNT = LGD_AMOUNT;
//        payVC.LGD_FINANCENAME = LGD_FINANCENAME;
//        payVC.cardno = cardno;
//        payVC.LGD_CARDINSTALLMONTH = LGD_CARDINSTALLMONTH;
        
        [self presentViewController:payVC animated:YES completion:nil];
//        [self dismissViewControllerAnimated:YES completion:^{
//            HomeVC *homeVC = (HomeVC*)self.parentViewController;
//            homeVC.data = payVC.data;
//            
//        }];
//        if([result isEqualToString: @"log"])
//        {
//            
//            UIViewController *ivc =
//            [(UINavigationController*)self.window.rootViewController pushViewController:ivc animated:NO];
//            NSLog(@"It's hitting log");
//        }
        
        //[self performSelector:NSSelectorFromString(functionName)];
        
        return NO;
    }

    
    //스마트 신한앱 다운로드 url
    NSString  *sh_url = @"http://itunes.apple.com/us/app/id360681882?mt=8";
    //신한Mobile앱 결제 다운로드 url
    NSString  *sh_url2= @"https://itunes.apple.com/kr/app/sinhan-mobilegyeolje/id572462317?mt=8";
    //현대 다운로드 url
    NSString  *hd_url = @"http://itunes.apple.com/kr/app/id362811160?mt=8";
    //스마트 신한 url 스키마
    NSString  *sh_appname = @"smshinhanansimclick";
    //스마트 신한앱 url 스키마
    NSString  *sh_appname2 = @"shinhan-sr-ansimclick";
    //현대카드 url
    NSString  *hd_appname = @"smhyundaiansimclick";
    //롯데카드 url
    NSString  *lottecard = @"lottesmartpay";
    
    NSLog(@"webview에 요청된 url==>%@",reqUrl);
    if ([reqUrl isEqualToString:hd_url] == YES ){
        NSLog(@"1. 현대 관련 url 입니다. ==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ( [reqUrl hasPrefix:hd_appname]){
        NSLog(@"2. 현대 관련 url 입니다.==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ([reqUrl isEqualToString:sh_url] == YES ){
        NSLog(@"1. 스마트신한 관련 url 입니다. ==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ( [reqUrl hasPrefix:sh_appname]){
        NSLog(@"2. 스마트신한 관련 url 입니다.==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ([reqUrl isEqualToString:sh_url2] == YES ){
        NSLog(@"1. 스마트신한앱 관련 url 입니다. ==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ( [reqUrl hasPrefix:sh_appname2]){
        NSLog(@"2. 신한모바일앱 관련 url 입니다.==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ( [reqUrl hasPrefix:lottecard]){
        NSLog(@"롯데카드 url 입니다.==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    //NH카드 공인인증서  다운로드 url
    NSString  *nh_url = @"https://itunes.apple.com/kr/app/nhansimkeullig/id609410702?mt=8";
    //nh카드 앱 url 스키마
    NSString  *nh_appname = @"nonghyupcardansimclick";
    if ([reqUrl isEqualToString:nh_url] == YES ){
        NSLog(@"NH앱 관련 url 입니다. ==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ( [reqUrl hasPrefix:nh_appname]){
        NSLog(@"NH 앱 관련 url 입니다.==>%@",reqUrl);
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            NSLog(@"cancel button");
            break;
        case 1:
        {
            switch (alertView.tag) {
                case 1:
                {
                    NSString *appLink = @"https://itunes.apple.com/kr/app/mobail-anjeongyeolje-isp/id369125087?mt=8";
                    NSURL *url = [NSURL URLWithString:appLink];
                    // AppStore 호출
                    [[UIApplication sharedApplication] openURL:url];
                }
                    break;
                case 2:{
                    NSString *appLink = @"https://itunes.apple.com/kr/app/seumateu-egseupei-gyejwaiche/id393794374?mt=8";
                    NSURL *url = [NSURL URLWithString:appLink];
                    // AppStore 호출
                    [[UIApplication sharedApplication] openURL:url];
                }
                default:
                    break;
            }
            
        }
            break;
            
        default:
            break;
    }
}
- (IBAction)check:(id)sender {
    //상점 URL 세팅 해주세요
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://page.ifou.co.kr/lguplus/payreq_crossplatform.php?LGD_OID=12345"]];
    //    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://pg.dacom.net:7080/cust/tf79/smartxpay/sample_crossplatform.html"]];
    
    [self.webview loadRequest:request];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
