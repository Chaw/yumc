//
//  ViewControllerPayWeb.h
//  yumc
//
//  Created by Wenly on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewControllerPayWeb : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webview;

- (IBAction)check:(id)sender;

@property (nonatomic, retain) NSString *DEPT_NAME;
//@property (nonatomic, retain) NSString *ACT_DATE;
@property (nonatomic, retain) NSString *CTNO; //CTNO		환자등록번호
@property (nonatomic, retain) NSString *PAY_ACT_AMT;
@property (nonatomic, retain) NSString *PROC_DATE; //PROC_DATE		처리일자
@property (nonatomic, retain) NSString *SEQ_NO;
@property (nonatomic, retain) NSString *CLNC_DEPT_CDE;
@property (nonatomic, retain) NSString *CARD_DATE_TIME;
@property (nonatomic, retain) NSString *CARD_APRV_NO;
@property (nonatomic, retain) NSString *CARD_NAME;
@property (nonatomic) int CARD_MON; // 할부개월
@property (nonatomic, retain) NSString *RCPT_DATE;
@property (nonatomic, retain) NSString *USERID;
@property (nonatomic, retain) NSString *EMAIL;
@property (nonatomic, retain) NSString *LGD_OID;
@property (nonatomic, retain) NSString *PRODUCTINFO;
@property (nonatomic, retain) NSString *USERNAME;
@property (nonatomic, retain) NSString *EASYPAY_ONLY;

@property (nonatomic, retain) NSString *ADM_CNT; // 중간금 입원회차
@property (nonatomic, retain) NSString *INSR_CLAS_CHNG_SEQ; // 중간금 변경순번
@property (nonatomic, retain) NSString *INSR_CLAS_CDE; // 중간금 변경순번
@property (nonatomic, retain) NSString *PAY_TYPE; // 중간금:M 외래:L
@property (nonatomic, retain) NSString *TREAT_TYPE; // 외래:O 입원:I

//@property (nonatomic, retain) NSString *READ_TIME; // 조회시간(사용하지 않음)
@property (nonatomic, retain) NSString *LIMIT_TIME; // 결제한도시간(초)

@property (nonatomic, retain) NSDate *UPDATE_DATE; // 목록 조회시간(READ_TIME 대체)


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

//SEQ_NO		순번

//ADM_CNT		중간금 입원회차
//INSR_CLAS_CHNG_SEQ		중간금 변경순번
//INSR_CLAS_CDE		중간금 구분
//PAY_TYPE		중간금:M 외래:L



//@property (nonatomic, retain) NSString *payInfo;
//@property (nonatomic, retain) NSString *oid;
//@property (nonatomic, retain) NSString *pName;
//@property (nonatomic, retain) NSString *treatDate;
//@property (nonatomic, retain) NSString *deptCode;
//@property (nonatomic, retain) NSString *payAmount;
@property (weak, nonatomic) IBOutlet UIImageView *imagaViewTitleIcon;

@end
