//
//  Url.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

class Url{
    static let API_BASE_URL = "https://yumcpay.yu.ac.kr/app/"
    

    /**
     * ============================================================================================
     * Login
     * ============================================================================================
     */
    
    // 회원 로그인
    static let LOGIN_USER = API_BASE_URL + "mLogin.php"
    // 비회원 로그인
    static let LOGIN_GUEST = API_BASE_URL + "mGuestLogin.php"
    
    
    /**
     * ============================================================================================
     * 외래수납
     * ============================================================================================
     */
    
    // 외래진료목록
    static let PAY_LIST = API_BASE_URL + "mPaylist.php"
    // 진료내역 리스트
    static let PAY_HISTORY = API_BASE_URL + "mPayHis.php"
    // 영수증 리스트
    static let RECEIPT = API_BASE_URL + "mReceipt.php"
    
    
    /**
     * ============================================================================================
     * 입원중간금수납
     * ============================================================================================
     */
    
    // 결제내역 리스트
    static let M_PAY_LIST = API_BASE_URL + "mMPaylist.php"
    // 진료내역 리스트
    static let M_PAY_HISTORY = API_BASE_URL + "mMPayHis.php"
    
    //static func DELETE_SEARCH_LOG(_ id : Int!) -> String! { return API_BASE_URL + "base/search/deactive/\(id!)/" }
    
    
    /**
     * ============================================================================================
     * 통신 지연 처리
     * ============================================================================================
     */
    // 5분 초과시 업데이트
    static let RECORD_TIMEOUT = API_BASE_URL + "mPayFiveLimmit.php"
    
}
