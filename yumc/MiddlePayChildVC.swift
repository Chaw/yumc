//
//  MiddlePayChildVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayChildVC: UIViewController, SendMiddlePayData, UITextFieldDelegate{
    

    @IBOutlet weak var middlePayChildTV: MiddlePayChildTV!
    @IBOutlet weak var middlePayChildDetailTV: MiddlePayChildDetailTV!
    @IBOutlet weak var hideTVButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var middlePayLastTV: MiddlePayLastTV!
    @IBOutlet weak var paymentTF: UITextField!
    var payment: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        middlePayChildTV.sendData = self
        middlePayChildTV.parentVC = self
        hideTVButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        hideTVButton.isHidden = true
        containerView.isHidden = true
        containerView2.isHidden = true
        middlePayChildDetailTV.layer.borderWidth = 1.0
        middlePayChildDetailTV.layer.borderColor = UIColor.lightGray.cgColor
        paymentTF.delegate = self
        paymentTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        payment = textField.text
        print(payment!)
    }
    
    @IBAction func hideDetailTV(_ sender: Any) {
        if !containerView2.isHidden {
            containerView2.isHidden = !containerView2.isHidden
        } else {
            hideTVButton.isHidden = true
            containerView.isHidden = true
            middlePayChildTV.list = [Payment2]()
            middlePayChildTV.reloadData()
            middlePayChildTV.load()
        }
    }
    
    @IBAction func showContainerView(_ sender: Any) {
        containerView2.isHidden = false
    }
    
    
    @IBAction func popPaynow(_ sender: Any) {
        
        if NumberFormatter().number(from: paymentTF.text!) != nil {
            // Register Nib
            let vc = ViewControllerPayWeb(nibName: "ViewControllerPayWeb", bundle: nil)
            let p:Payment2? = middlePayChildDetailTV.list
            
            vc.insr_CLAS_CDE = p?.INSR_CLAS_CDE
            vc.adm_CNT = p?.ADM_CNT
            vc.insr_CLAS_CHNG_SEQ = p?.INSR_CLAS_CHNG_SEQ
            vc.clnc_DEPT_CDE = p?.CLNC_DEPT_CDE
            vc.pay_TYPE = "M"
            vc.treat_TYPE = "I"
            vc.update_DATE = middlePayChildTV.updateDate
            vc.pay_ACT_AMT = payment
            vc.seq_NO = p?.SEQ_NO
            vc.proc_DATE = p?.PROC_DATE
            vc.ctno = UserData.userno
            vc.card_MON = Int32(middlePayLastTV.month)
            vc.easypay_ONLY = "PAYNOW"
            
            vc.userid = UserData.loginId
            vc.username = UserData.usr_name
            
            // Present View "Modally"
            self.parent?.present(vc, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "알림", message: "금액을 입력하세요.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func popPayWebView(_ sender: Any) {
        
        if NumberFormatter().number(from: paymentTF.text!) != nil {
            // Register Nib
            let vc = ViewControllerPayWeb(nibName: "ViewControllerPayWeb", bundle: nil)
            let p:Payment2? = middlePayChildDetailTV.list
            
            vc.insr_CLAS_CDE = p?.INSR_CLAS_CDE
            vc.adm_CNT = p?.ADM_CNT
            vc.insr_CLAS_CHNG_SEQ = p?.INSR_CLAS_CHNG_SEQ
            vc.clnc_DEPT_CDE = p?.CLNC_DEPT_CDE
            vc.pay_TYPE = "M"
            vc.treat_TYPE = "I"
            vc.update_DATE = middlePayChildTV.updateDate
            vc.pay_ACT_AMT = payment
            vc.seq_NO = p?.SEQ_NO
            vc.proc_DATE = p?.PROC_DATE
            vc.ctno = UserData.userno
            vc.card_MON = Int32(middlePayLastTV.month)
            vc.easypay_ONLY = ""
            
            vc.userid = UserData.loginId
            vc.username = UserData.usr_name
            
            // Present View "Modally"
            self.parent?.present(vc, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "알림", message: "금액을 입력하세요.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            
            
            self.present(alert, animated: true, completion: nil)
            
            
            
            
        }
        
//        if(paymentTF.text ?.isEmpty){
//            // Register Nib
//            let vc = ViewControllerPayWeb(nibName: "ViewControllerPayWeb", bundle: nil)
//            let p:Payment2? = middlePayChildDetailTV.list
//            
//            vc.insr_CLAS_CDE = p?.INSR_CLAS_CDE
//            vc.adm_CNT = p?.ADM_CNT
//            vc.insr_CLAS_CHNG_SEQ = p?.INSR_CLAS_CHNG_SEQ
//            vc.clnc_DEPT_CDE = p?.CLNC_DEPT_CDE
//            vc.pay_TYPE = "M"
//            vc.pay_ACT_AMT = payment
//            vc.seq_NO = p?.SEQ_NO
//            vc.proc_DATE = p?.PROC_DATE
//            vc.ctno = UserData.userno
//            
//            vc.userid = UserData.loginId
//            vc.username = UserData.usr_name
//            
//            // Present View "Modally"
//            self.parent?.present(vc, animated: true, completion: nil)
//        }
//        else{
//            let alert = UIAlertController(title: "알림", message: "금액을 입력하세요.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
//            
//            
//            self.present(alert, animated: true, completion: nil)
//            
//            
//            paymentTF.becomeFirstResponder()
//            
//        }
        
        
    }
    
    
    func sendData(_ data: Payment2) {
        middlePayChildDetailTV.list = data
        middlePayLastTV.list = data
        middlePayChildDetailTV.reloadData()
        middlePayLastTV.reloadData()
        containerView.isHidden = false
        hideTVButton.isHidden = false
    }

    @IBAction func popSetting(_ sender: Any) {
        SettingVC.popUpSetting(self)
    }
    
    @IBOutlet weak var tvMsg: UITextView!
    
    @IBAction func actionCallM(_ sender: Any) {
        if let url = URL(string:"telprompt://053-620-4527"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}
