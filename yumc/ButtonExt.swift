//
//  ButtonExt.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

extension UIButton{
    func imageCenter (_ spacing: CGFloat) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + spacing
        
        if imageSize.width < titleSize.width {
            self.contentEdgeInsets = UIEdgeInsets(
                top: imageSize.height / 2.0,
                left: -imageSize.width / 2.0,
                bottom: imageSize.height / 2.0,
                right: -imageSize.width / 2.0
            )
        } else {
            self.contentEdgeInsets = UIEdgeInsets(
                top: imageSize.height / 2.0,
                left: -titleSize.width / 2.0,
                bottom: imageSize.height / 2.0,
                right: -titleSize.width / 2.0
            )
        }
        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            left: 0,
            bottom: 0,
            right: -titleSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: 0
        )
        
    }
}
