//
//  MiddlePayHIstoryVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MiddlePayHistoryVC: UIViewController, IndicatorInfoProvider{
    
    var itemInfo: IndicatorInfo = "입원중간금결제내역"
    
    static func instantiate(_ itemInfo: IndicatorInfo) -> MiddlePayHistoryVC{
        let vc = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "middlePayHistoryVC") as! MiddlePayHistoryVC
        vc.itemInfo = itemInfo
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
