//
//  PopBarcodeVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PopBarcodeVC: UIViewController{
    var barcodeImage: UIImage?
    var barcodeString: String?
    @IBOutlet weak var barcodeView: UIView!
    @IBOutlet weak var barcodeImageView: UIImageView!
    @IBOutlet weak var popBarView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        print(barcodeImageView.frame.origin.x)
        barcodeView.layer.cornerRadius = 10.0
        let X = barcodeImageView.frame.origin.x
        let Y = barcodeImageView.frame.origin.y
        self.barcodeImageView.transform = CGAffineTransform(rotationAngle: CGFloat(90).degreesToRadians)
        print(X)
        self.barcodeImageView.frame = CGRect(x: X, y: Y, width: self.barcodeImageView.frame.height, height: self.barcodeImageView.frame.width)
        let barcode = Barcode.fromString(barcodeString!, scaleX : 18.0, scaleY : 26.0)
        barcodeImageView.image = barcode
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func degreeToRadian(_ angle: Float){
        
    }
}
extension Int{
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint{
    var degreesToRadians: Self { return self * .pi / 180 }
}
