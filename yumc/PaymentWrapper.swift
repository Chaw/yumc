//
//  Payment.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

//{
//    "PTNM": "ìµíêµ",
//    "ListData": [{
//    "DEPT_NAME": "\ud638\ud761\uae30\uc9c4\ub8cc\uc13c\ud130",
//    "ACT_DATE": "20170125",
//    "CTNO": "20004832",
//    "PAY_ACT_AMT": "191140",
//    "PROC_DATE": "20170125",
//    "SEQ_NO": "1"
//}],
//"result_code": "100",
//"result_msg": "ì ìì²ë¦¬"
//}

import ObjectMapper

class PaymentWrapper: Mappable {
    var PTNM: String?
    var ListData: [Payment]?
    var result_code: String?
    var result_msg: String?
    var calc_fg: String?
    var calc_msg: String?
    
    func mapping(map: Map) {
        PTNM <- map["PTNM"]
        ListData <- map["ListData"]
        result_code <- map["result_code"]
        result_msg <- map["result_msg"]
        calc_fg <- map["calc_fg"]
        calc_msg <- map["calc_msg"]
    }
    
    required init?(map: Map) {
    }

    static func getPaymentList(
        _ userno:String!,
        completion: @escaping (_ object:PaymentWrapper) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
        param["userno"] = userno
        API.getRequest(Url.PAY_LIST,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
    
    static func getPaymentHistory(
        _ usrno:String!,
        completion: @escaping (_ object:PaymentWrapper) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
        param["userno"] = usrno
        API.getRequest(Url.PAY_HISTORY,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
}
