//
//  MiddlePayHistoryCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayHistoryCell: UITableViewCell{
    
    @IBOutlet weak var CARD_BANK: UILabel!
    @IBOutlet weak var CHAR_CARD_DATE_TIME: UILabel!
    @IBOutlet weak var CARD_APRV_NO: UILabel!
    @IBOutlet weak var CARD_MON: UILabel!
    @IBOutlet weak var CARD_AMT: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
