//
//  PayChildCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayChildCell: UITableViewCell{
    @IBOutlet weak var treatDateLabel: UILabel!
    @IBOutlet weak var departureNameLabel: UILabel!
    @IBOutlet weak var registerNumLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
