//
//  MiddlePayChildDetailCell.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayChildDetailCell: UITableViewCell{
    
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var totalAmtLabel: UILabel!
    @IBOutlet weak var insrAmtLabel: UILabel!
    @IBOutlet weak var chrgAmtLabel: UILabel!
    @IBOutlet weak var admAmtLabel: UILabel!
    @IBOutlet weak var payAmtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

