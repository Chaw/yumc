//
//  Payment2His.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

//{
//    "CTNO": "17899872",
//    "CARD_AMT": "1000",
//    "CHAR_CARD_DATE_TIME": "20170214010225",
//    "CARD_APRV_NO": "41704765",
//    "CARD_BANK": "\ube44\uc528\uce74\ub4dc",
//    "CARD_MON": "\uc77c\uc2dc\ubd88"
//}

import ObjectMapper

class Payment2His: Mappable{
    
    var CTNO: String?
    var CARD_AMT: String?
    var CHAR_CARD_DATE_TIME: String?
    var CARD_APRV_NO: String?
    var CARD_BANK: String?
    var CARD_MON: String?
    
    func mapping(map: Map) {
        CTNO <- map["CTNO"]
        CARD_AMT <- map["CARD_AMT"]
        CHAR_CARD_DATE_TIME <- map["CHAR_CARD_DATE_TIME"]
        CARD_APRV_NO <- map["CARD_APRV_NO"]
        CARD_BANK <- map["CARD_BANK"]
        CARD_MON <- map["CARD_MON"]
    }
    
    required init?(map: Map) {
    }
}
