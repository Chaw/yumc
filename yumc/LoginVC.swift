//
//  LoginVC.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 22..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class LoginVC: UIViewController{
    
    @IBOutlet weak var idTF: UITextField!
    @IBOutlet weak var pwTF: UITextField!
    @IBOutlet weak var autoLoginImage: UIImageView!
    @IBOutlet weak var saveInfoImage: UIImageView!
    var autoLoginBool: Bool?
    var saveInfo: Bool?
    var autoLogined = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkUserDefault()
        checkSaveInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        checkUserDefault()
        checkSaveInfo()
        checkAutoLogin(autoLogined)
        autoLogined = true
    }
    
    @IBAction func idTextDel(_ sender: Any) {
        idTF.text = ""
    }
    @IBAction func pwTextDel(_ sender: Any) {
        pwTF.text = ""
    }
    
    @IBAction func autoLoginTap(_ sender: Any) {
        
        autoLoginBool = !autoLoginBool!
        checkAutoLogin(true)
    }
    
    @IBAction func saveInfoTap(_ sender: Any) {
        UserData.isSaveId = !saveInfo!
        saveInfo = !saveInfo!
        checkSaveInfo()
    }
    
    @IBAction func goToCustomerLogin(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "customerLoginVC")
        self.present(vc!, animated: false, completion: nil)
    }
    
    func checkAutoLogin(_ fromUser: Bool!){
        if autoLoginBool! {
            autoLoginImage.image = UIImage(named: "circle_check_s.png")
            
            if let _ = UserData.loginId {
                if !fromUser{
                    //toMain()
                    idTF.text = UserData.loginId
                    pwTF.text = UserData.loginPw
                    actionLogin(self)
                }
                return
            }
            if let _ = UserData.patientNumber {
                if !fromUser{
                    //toMain()
                    idTF.text = UserData.loginId
                    pwTF.text = UserData.loginPw
                    actionLogin(self)
                }
                return
            }
        } else {
            autoLoginImage.image = UIImage(named: "circle_check_n.png")
        }
    }
    func checkSaveInfo(){
        if saveInfo! {
            saveInfoImage.image = UIImage(named: "circle_check_s.png")
            if let loginId = UserData.loginId {
                idTF.text = loginId
            }
        } else {
            saveInfoImage.image = UIImage(named: "circle_check_n.png")
            idTF.text = ""
        }
    }
    
    func checkUserDefault(){
        saveInfo = UserData.isSaveId
        autoLoginBool = UserData.isAutoLogin
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        
        UserData.isAutoLogin = autoLoginBool!
        
        Login.loginWithUser(
            idTF.text,
            usr_pw: pwTF.text,
            completion: { (object:Login) in
                if object.result_code == "100"{
                    UserData.setLogin(true, loginId: self.idTF.text, loginPw: self.pwTF.text, userno: object.usr_no, usr_name: object.usr_name)
                    self.toMain()
                }
                else {
                    UserData.setLogin(false, loginId: self.idTF.text, loginPw: self.pwTF.text, userno: nil, usr_name: nil)
                    
                    let alert = UIAlertController(title: "알림", message: object.result_msg?.decodeUri(), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }, indicator: self.view)
    }
    
    func toMain() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
        vc?.modalTransitionStyle = .crossDissolve;
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.homeVC = vc
        self.present(vc!, animated: true, completion: nil)
    }
}
