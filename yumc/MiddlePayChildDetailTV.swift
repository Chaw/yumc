//
//  MiddlePayChildDetailTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayChildDetailTV: UITableView, UITableViewDataSource, UITableViewDelegate{
    
    var list: Payment2?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "MiddlePayChildDetailCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 300
        self.rowHeight = UITableViewAutomaticDimension
        
        if list != nil {
            print(list!)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list == nil{
            return 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiddlePayChildDetailCell
        cell.startDateLabel.text = list!.DUR_START?.makeDateForm()
        cell.endDateLabel.text = list!.DUR_END?.makeDateForm()
        if let totalAmt = Int((list?.TOT_AMT)!){
            cell.totalAmtLabel.text = totalAmt.addComma()
        } else {
            cell.totalAmtLabel.text = ""
        }
        if let insrAmt = Int((list?.INSR_AMT)!){
            cell.insrAmtLabel.text = insrAmt.addComma()
        } else {
            cell.insrAmtLabel.text = ""
        }
        if let chrgAmt = Int(list!.CHRG_AMT!){
            cell.chrgAmtLabel.text = chrgAmt.addComma()
        } else {
            cell.chrgAmtLabel.text = ""
        }
        if let admAmt = Int(list!.ADM_AMT!){
            cell.admAmtLabel.text = admAmt.addComma()
        } else {
            cell.admAmtLabel.text = ""
        }
        if let payAmt = Int(list!.AMNG_AMT!){
            cell.payAmtLabel.text = payAmt.addComma()
        } else {
            cell.payAmtLabel.text = ""
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
}
