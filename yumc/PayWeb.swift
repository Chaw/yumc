//
//  LoginUser.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import ObjectMapper


class PayWeb: Mappable{
    
    var CST_MID: String?
    var CST_PLATFORM: String?
    var LGD_NOINT: String?
    var LGD_KVPMISPAUTOAPPYN: String?
    var LGD_MONEPAY_AUTORUNYN: String?
    var LGD_PRODUCTINFO: String?
    var LGD_OID: String?
    var LGD_BUYER: String?
    var LGD_BUYERID: String?
    var LGD_BUYEREMAIL: String?
    var LGD_AMOUNT: String?
    var LGD_CUSTOM_FIRSTPAY: String?
    var LGD_EASYPAY_ONLY: String?
    var LGD_INSTALL: String?
    var PROC_DATE: String?
    var SEQ_NO: String?
    var USERNO: String?
    var CLNC_DEPT_CDE: String?
    var PAY_TYPE: String?
    
    func mapping(map: Map) {
        
        CST_MID <- map["CST_MID"]
        CST_PLATFORM <- map["CST_PLATFORM"]
        LGD_NOINT <- map["LGD_NOINT"]
        LGD_KVPMISPAUTOAPPYN <- map["LGD_KVPMISPAUTOAPPYN"]
        LGD_MONEPAY_AUTORUNYN <- map["LGD_MONEPAY_AUTORUNYN"]
        LGD_PRODUCTINFO <- map["LGD_PRODUCTINFO"]
        LGD_OID <- map["LGD_OID"]
        LGD_BUYER <- map["LGD_BUYER"]
        LGD_BUYERID <- map["LGD_BUYERID"]
        LGD_BUYEREMAIL <- map["LGD_BUYEREMAIL"]
        LGD_AMOUNT <- map["LGD_AMOUNT"]
        LGD_CUSTOM_FIRSTPAY <- map["LGD_CUSTOM_FIRSTPAY"]
        LGD_EASYPAY_ONLY <- map["LGD_EASYPAY_ONLY"]
        LGD_INSTALL <- map["LGD_INSTALL"]
        PROC_DATE <- map["PROC_DATE"]
        SEQ_NO <- map["SEQ_NO"]
        USERNO <- map["USERNO"]
        CLNC_DEPT_CDE <- map["CLNC_DEPT_CDE"]
        PAY_TYPE <- map["PAY_TYPE"]
//        result_code <- map["result_code"]
//        result_msg <- map["result_msg"]
//        
    }
    
    required init?(map: Map) {
    }
    
    static func pay(
        _ CST_MID: String!,
        CST_PLATFORM: String!,
        LGD_NOINT: String!,
        LGD_KVPMISPAUTOAPPYN: String!,
        LGD_MONEPAY_AUTORUNYN: String!,
        LGD_PRODUCTINFO: String!,
        LGD_OID: String!,
        LGD_BUYER: String!,
        LGD_BUYERID: String!,
        LGD_BUYEREMAIL: String!,
        LGD_AMOUNT: String!,
        LGD_CUSTOM_FIRSTPAY: String!,
        LGD_EASYPAY_ONLY: String!,
        LGD_INSTALL: String!,
        PROC_DATE: String!,
        SEQ_NO: String!,
        USERNO: String!,
        CLNC_DEPT_CDE: String!,
        PAY_TYPE: String!,
        completion: @escaping (_ object:PayWeb) -> Void,
        indicator:UIView?) -> Void {
        
        var param = [String:Any]()
//        param["usr_id"] = usr_id
//        param["usr_pw"] = usr_pw
        API.getRequest(Url.LOGIN_USER,
                       parameters: param,
                       completion: completion,
                       indicator: indicator)
    }
}
