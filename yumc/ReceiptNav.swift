//
//  ReceiptNav.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Foundation

class ReceiptNav: UINavigationController {
    override func viewWillAppear(_ animated: Bool) {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
}
