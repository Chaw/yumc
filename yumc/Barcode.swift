//
//  Barcode.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 23..
//  Copyright © 2017년 gaon. All rights reserved.
//

import CoreImage
import UIKit

class Barcode {
    
    class func fromString(_ string : String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CICode128BarcodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        let input = filter?.outputImage!
        let ouput = input?.applying(CGAffineTransform(scaleX: 18.0, y: 18.0))
//        let filter2 = CIFilter(name: "CILanczosScaleTransform")!
//        filter2.setValue(filter!.outputImage, forKey: "inputImage")
//        filter2.setValue(1.0, forKey: "inputScale")
//        filter2.setValue(1.0, forKey: "inputAspectRatio")
//        let outputImage = filter2.value(forKey: "outputImage") as! CIImage
//        let context = CIContext(options: [kCIContextUseSoftwareRenderer: false])
//        let scaledImage = UIImage(cgImage: context.createCGImage(outputImage, from: outputImage.extent)!)
//        
//        return scaledImage
//        return UIImage(ciImage: (filter?.outputImage!)!)
        return UIImage(ciImage: ouput!, scale: 1.0, orientation: UIImageOrientation.up)
//        return UIImage(ciImage: ouput!)
        
    }
    
    class func fromString(_ string : String, scaleX : CGFloat, scaleY : CGFloat) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CICode128BarcodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        let input = filter?.outputImage!
        let ouput = input?.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        //        let filter2 = CIFilter(name: "CILanczosScaleTransform")!
        //        filter2.setValue(filter!.outputImage, forKey: "inputImage")
        //        filter2.setValue(1.0, forKey: "inputScale")
        //        filter2.setValue(1.0, forKey: "inputAspectRatio")
        //        let outputImage = filter2.value(forKey: "outputImage") as! CIImage
        //        let context = CIContext(options: [kCIContextUseSoftwareRenderer: false])
        //        let scaledImage = UIImage(cgImage: context.createCGImage(outputImage, from: outputImage.extent)!)
        //
        //        return scaledImage
        //        return UIImage(ciImage: (filter?.outputImage!)!)
        return UIImage(ciImage: ouput!, scale: 1.0, orientation: UIImageOrientation.up)
        //        return UIImage(ciImage: ouput!)
        
    }

    
    class func fromStringRotated(_ string : String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CICode128BarcodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        let input = filter?.outputImage!
        let ouput = input?.applying(CGAffineTransform(scaleX: 18.0, y: 18.0))
        //        let filter2 = CIFilter(name: "CILanczosScaleTransform")!
        //        filter2.setValue(filter!.outputImage, forKey: "inputImage")
        //        filter2.setValue(1.0, forKey: "inputScale")
        //        filter2.setValue(1.0, forKey: "inputAspectRatio")
        //        let outputImage = filter2.value(forKey: "outputImage") as! CIImage
        //        let context = CIContext(options: [kCIContextUseSoftwareRenderer: false])
        //        let scaledImage = UIImage(cgImage: context.createCGImage(outputImage, from: outputImage.extent)!)
        //
        //        return scaledImage
        //        return UIImage(ciImage: (filter?.outputImage!)!)
        return UIImage(ciImage: ouput!, scale: 1.0, orientation: .right)
        
    }
}
