//
//  PayHistoryTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class PayHistoryTV: UITableView, UITableViewDelegate, UITableViewDataSource{
    
    var data = [[String:Any]]()
    var list = [Payment]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "PayHistoryCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 80
        self.rowHeight = UITableViewAutomaticDimension
        
        PaymentWrapper.getPaymentHistory(
            UserData.userno,
            completion: { (object:PaymentWrapper) in
                if object.result_code == "100" {
                    self.list = object.ListData!
                    self.reloadData()
                    if self.list.count == 0{
                        self.isHidden = true
                    }
                } else {
                    self.isHidden = true
                }
        }, indicator: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 진료일자
        // 진료과
        // 결제일시
        // 승인번호 결제기관
        // 할부개월 결제금액
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PayHistoryCell
        let p:Payment = list[indexPath.row]
        
        if let ACT_DATE = p.ACT_DATE {
            cell.ACT_DATE.text = ACT_DATE.makeDateForm()
        } else {
            cell.ACT_DATE.text = ""
        }
        
        if let DEPT_NAME = p.DEPT_NAME {
            cell.DEPT_NAME.text = DEPT_NAME
        } else {
            cell.DEPT_NAME.text = ""
        }
        
        if let CARD_DATE_TIME = p.CARD_DATE_TIME {
            cell.CARD_DATE_TIME.text = CARD_DATE_TIME.makeDatetimeForm()
        } else {
            cell.CARD_DATE_TIME.text = ""
        }
        
        if let CARD_APRV_NO = p.CARD_APRV_NO {
            cell.CARD_APRV_NO.text = CARD_APRV_NO
        } else {
            cell.CARD_APRV_NO.text = ""
        }
        
        if let CARD_NAME = p.CARD_NAME {
            cell.CARD_NAME.text = CARD_NAME
        } else {
            cell.CARD_NAME.text = ""
        }
        
        if let CARD_MON = p.CARD_MON {
            cell.CARD_MON.text = CARD_MON
        } else {
            cell.CARD_MON.text = ""
        }
        
        if let PAY_ACT_AMT = p.PAY_ACT_AMT {
            cell.PAY_ACT_AMT.text = Int(PAY_ACT_AMT)?.addComma()
        } else {
            cell.PAY_ACT_AMT.text = ""
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let index = indexPath.row
//        let selectedData = data[index]
        let p:Payment = list[indexPath.row]
        
        let vc = self.viewController()
        let vcReceiptNav = vc?.storyboard?.instantiateViewController(withIdentifier: "ReceiptNavVC") as! UINavigationController
        let vcReceipt = vcReceiptNav.topViewController as! ReceiptVC
        vcReceipt.payment = p
        vc?.present(vcReceiptNav, animated: false, completion: nil)
        
    }

}
