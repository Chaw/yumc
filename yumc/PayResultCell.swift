//
//  PayResultCell.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 17..
//  Copyright © 2017년 gaon. All rights reserved.
//

class PayResultCell: UITableViewCell{
    
    @IBOutlet weak var methodLabel: UILabel!
    @IBOutlet weak var payDateLabel: UILabel!
    @IBOutlet weak var approveLabel: UILabel!
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var payLabel: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var monLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
