//
//  MiddlePayHistoryTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

class MiddlePayHistoryTV: UITableView, UITableViewDelegate, UITableViewDataSource{
    
    var data = [[String:Any]]()
    var list = [Payment2His]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "MiddlePayHistoryCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 80
        self.rowHeight = UITableViewAutomaticDimension
//        data = [["dep": "신경외과", "date":"2017.01.10","pay":"50,000원","number":"1", "period":"2017.01.06~2017.01.08"],
//                ["dep": "신경과", "date":"2017.01.15","pay":"40,000원","number":"2", "period":"2017.01.06~2017.01.18"],
//                ["dep": "외과", "date":"2017.01.20","pay":"5,000원","number":"3", "period":"2017.01.06~2017.02.08"],
//                ["dep": "내과", "date":"2017.01.30","pay":"500,000원","number":"4", "period":"2017.01.06~2017.03.08"]
//        ]
        PaymentWrapper2His.getPaymentHistory(
            UserData.userno,
            completion: { (object:PaymentWrapper2His) in
                if object.result_code == "100" {
                    self.list = object.ListData!
                    self.reloadData()
                    if self.list.count == 0{
                        self.isHidden = true
                    }
                } else {
                    self.isHidden = true
                }
        }, indicator: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiddlePayHistoryCell
        let p:Payment2His = list[indexPath.row]
        
        if let CARD_BANK = p.CARD_BANK {
            cell.CARD_BANK.text = CARD_BANK
        } else {
            cell.CARD_BANK.text = ""
        }
        
        if let CHAR_CARD_DATE_TIME = p.CHAR_CARD_DATE_TIME {
            cell.CHAR_CARD_DATE_TIME.text = CHAR_CARD_DATE_TIME.makeDatetimeForm()
        } else {
            cell.CHAR_CARD_DATE_TIME.text = ""
        }
        
        if let CARD_APRV_NO = p.CARD_APRV_NO {
            cell.CARD_APRV_NO.text = CARD_APRV_NO
        } else {
            cell.CARD_APRV_NO.text = ""
        }
        
        if let CARD_MON = p.CARD_MON {
            cell.CARD_MON.text = CARD_MON
        } else {
            cell.CARD_MON.text = ""
        }
        
        if let CARD_AMT = p.CARD_AMT {
            cell.CARD_AMT.text = Int(CARD_AMT)?.addComma()
        } else {
            cell.CARD_AMT.text = ""
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let index = indexPath.row
        //        let selectedData = data[index]
    }
    
}
