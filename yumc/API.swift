//
//  API.swift
//  yumc
//
//  Created by 성찬 윤 on 2017. 2. 16..
//  Copyright © 2017년 gaon. All rights reserved.
//

import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class API {
    static let DEBUG: Bool = true
    
    static var headers: [String : String] {
        return [String : String]()
    }
    
    static func baseRequest(
        _ method:Alamofire.HTTPMethod,
        url:String!,
        parameters:[String : Any],
        encoding:Alamofire.ParameterEncoding,
        indicator:UIView?) -> DataRequest {
        
        startIndicator(indicator)
        
        return Alamofire.request(
            url,
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: self.headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
    }
    
    static func getRequest<T:Mappable>(
        _ url:String!,
        parameters:[String : Any],
        completion: @escaping (_ object:T) -> Void,
        indicator:UIView?) -> Void {
        
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForResource = 5 // seconds
        
//        let alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
        let request = baseRequest(.get, url: url, parameters: normParams(parameters), encoding: URLEncoding.queryString, indicator: indicator)
        request.responseObject{ (response: DataResponse<T>) in
            handleResponse(request, response: response, completion: completion, indicator: indicator)
        }
    }
    
    static func getRequestArray<T:Mappable>(
        _ url:String!,
        parameters:[String : Any],
        completion: @escaping (_ object:[T]) -> Void,
        indicator:UIView) -> Void {
        
        let request = baseRequest(.get, url: url, parameters: normParams(parameters), encoding: URLEncoding.queryString, indicator: indicator)
        request.responseArray{ (response: DataResponse<[T]>) in
            handleResponseArray(request, response: response, completion: completion, indicator: indicator)
        }
    }
    
    static func postRequest<T:Mappable>(
        _ url:String!,
        parameters:[String : Any],
        completion: @escaping (_ object:T) -> Void,
        indicator:UIView) -> Void {
        
        let request = baseRequest(.post, url: url, parameters: parameters, encoding: JSONEncoding.prettyPrinted, indicator: indicator)
        //        let request = baseRequest(.post, url: url, parameters: parameters, encoding: .JSON, indicator: indicator)
        request.responseObject{ (response: DataResponse<T>) in
            handleResponse(request, response: response, completion: completion, indicator: indicator)
        }
    }
    
    static func putRequest<T:Mappable>(
        _ url:String!,
        parameters:[String : Any],
        completion: @escaping (_ object:T) -> Void,
        indicator:UIView) -> Void {
        
        let request = baseRequest(.put, url: url, parameters: parameters, encoding: JSONEncoding.prettyPrinted, indicator: indicator)
        //        let request = baseRequest(.put, url: url, parameters: parameters, encoding: .JSON, indicator: indicator)
        request.responseObject{ (response: DataResponse<T>) in
            handleResponse(request, response: response, completion: completion, indicator: indicator)
        }
    }
    
    static func deleteRequest(
        _ url:String!,
        completion: @escaping () -> Void,
        indicator:UIView) -> Void {
        
        let request = baseRequest(.delete, url: url, parameters: [:], encoding: JSONEncoding.prettyPrinted, indicator: indicator)
        //        let request = baseRequest(.delete, url: url, parameters: [:], encoding: .JSON, indicator: indicator)
        request.response { (response) in
            if DEBUG {
                debugPrint(response.request!)
                debugPrint(response.response!)
            }
            
            stopIndicator(indicator)
            completion()
        }
    }
    
    static func upload<T:Mappable>(
        _ url:String!,
        method:Alamofire.HTTPMethod,
        files:[URL],
        jsonObject:Mappable!,
        completion: @escaping (_ object:T) -> Void,
        indicator:UIView
        ) -> Void {
        
        startIndicator(indicator)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                let json = jsonObject.toJSONString()!.data(using: String.Encoding.utf8)!
                for (i, file) in files.enumerated() {
                    var key: String!
                    if i == 0 {
                        key = "file"
                    } else {
                        key = "file\(i + 1)"
                    }
                    multipartFormData.append(file, withName: key)
                }
                multipartFormData.append(json, withName: "json")
        },
            to: url,
            method: method,
            headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseObject(completionHandler: { (response:DataResponse<T>) in
                        if DEBUG {
                            debugPrint(response)
                        }
                        
                        stopIndicator(indicator)
                        completion(response.result.value!)
                    })
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    static func handleResponse<T:Mappable>(_ request:Request, response:DataResponse<T>, completion: (_ object:T) -> Void, indicator:UIView?) -> Void {
        if DEBUG {
            debugPrint(request)
            debugPrint(response)
        }
        
        stopIndicator(indicator)
        
        if response.result.isSuccess {
            if DEBUG {
                print(NSString(data:response.data!, encoding: String.Encoding.utf8.rawValue)!)
            }
            
            completion(response.result.value!)
        }
    }
    
    static func handleResponseArray<T:Mappable>(_ request:Request, response:DataResponse<[T]>, completion: (_ object:[T]) -> Void, indicator:UIView) -> Void {
        if DEBUG {
            debugPrint(request)
            debugPrint(response)
        }
        
        stopIndicator(indicator)
        
        if response.result.isSuccess {
            if DEBUG {
                print(NSString(data:response.data!, encoding: String.Encoding.utf8.rawValue)!)
            }
            
            completion(response.result.value!)
        }
    }
    
    static func pageParameter(_ page: Int!) -> [String : Any] {
        return ["page" : page as AnyObject]
    }
    
    static func normParams(_ parameters:[String:Any]) -> [String:Any] {
        var newParams = parameters
        
        for p in newParams {
            if let _ = p.1 as? String{
                
            } else {
                if let object = p.1 as? [Int] {
                    var string = ""
                    for o in object {
                        string = string + "\(o),"
                    }
                    newParams[p.0] = string as Any?
                }
            }
        }
        return newParams
    }
    
    static func startIndicator(_ indicator: UIView?){
        if let view = indicator{
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            view.addSubview(activityIndicator)
            view.isUserInteractionEnabled = false
            
            let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
            view.addConstraint(horizontalConstraint)
            
            let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
            view.addConstraint(verticalConstraint)
            
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.startAnimating()
        }
    }
    
    static func stopIndicator(_ indicator: UIView?){
        if let view = indicator{
            view.isUserInteractionEnabled = true
            for i in view.subviews{
                if i.isKind(of: UIActivityIndicatorView.self){
                    (i as! UIActivityIndicatorView).stopAnimating()
                    i.isHidden = true
                    i.removeFromSuperview()
                }
            }
        }
    }
}
