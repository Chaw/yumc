//
//  MiddlePayChildTV.swift
//  yumc
//
//  Created by 조지훈 on 2017. 1. 24..
//  Copyright © 2017년 gaon. All rights reserved.
//

import UIKit

protocol SendMiddlePayData{
    func sendData(_ data : Payment2)
}

class MiddlePayChildTV: UITableView, UITableViewDelegate, UITableViewDataSource{
    
    var list:[Payment2]?
    var parentVC:MiddlePayChildVC?
    var updateDate: Date?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        initialize()
    }
    
    func initialize(){
        let nib = UINib(nibName: "MiddlePayChildCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        self.estimatedRowHeight = 300
        self.rowHeight = UITableViewAutomaticDimension
        list = [Payment2]()
        load()
    }
    
    func load(){
        PaymentWrapper2.getPaymentList(UserData.userno, completion: { (object) in
            if object.result_code == "100" {
                self.list = object.ListData
                self.reloadData()
                if self.list?.count == 0{
                    self.isHidden = true
                }
                
            } else {
                self.isHidden = true
            }
            
            if object.calc_fg == "N" {
                let alert = UIAlertController(title: "알림", message: object.result_msg?.decodeUri(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.isHidden = true
                self.parentVC?.present(alert, animated: true, completion: nil)
                self.parentVC?.tvMsg.text =  object.result_msg?.decodeUri()
                self.parentVC?.tvMsg.isHidden = false
                
            }
            else{
                self.isHidden = false
                self.parentVC?.tvMsg.isHidden = true
            }

            self.updateDate = Date()
            
        }, indicator: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if list?.count != 0 {
            return list!.count
        } else {
            return 0
        }
    }
    var sendData: SendMiddlePayData?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MiddlePayChildCell
        let item = list?[indexPath.row]
        cell.startDateLabel.text = item?.DUR_START?.makeDateForm()
        cell.endDateLabel.text = item?.DUR_END?.makeDateForm()
        cell.insuranceLabel.text = item?.INSR_CLAS_CDE_NM?.decodeUri()
        
        if let totalPayment = Int(item!.CHRG_AMT!){
            cell.totalPaymentLabel.text = totalPayment.addComma()
        } else {
            cell.totalPaymentLabel.text = ""
        }
        if let middlePayment = item?.RECV_AMT{
            cell.middlePaymentLabel.text = Int(middlePayment)?.addComma()
        } else {
            cell.middlePaymentLabel.text = ""
        }
        if let payment = item?.AMNG_AMT{
            cell.paymentLabel.text = Int(payment)?.addComma()
        } else {
            cell.paymentLabel.text = ""
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let selectedData = list?[index]
        sendData?.sendData(selectedData!)
    }
}
